@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=20
REM -----------------

set d=128
echo %CONSOLE_DIR%
for /l %%x in (1, 1, 32) do (

	echo Liczenie obrazu calkowego w blokiach !d! kolumny na !d! wiersze 
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolnaiveobgpu 512 512 %ITERATIONS% /generate !d! !d! /discard >> testy\naive_vs_basket\nob.csv
	
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolbasketobgpu 512 512 %ITERATIONS% /generate !d! !d! /discard >> testy\naive_vs_basket\bob.csv
	
	set /A d=!d!+128
)


ENDLOCAL