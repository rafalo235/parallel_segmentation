@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=100
REM -----------------

set d=2
rm defcpu.csv
rm rowcolitergpu.csv
rm rowcolnaivegpu.csv
rm rowcolbasketgpu.csv
rm rowcolefficientgpu.csv
echo %CONSOLE_DIR%
for /l %%x in (1, 1, 12) do (
	echo Binaryzacja obrazu o rozmiarach !d! na !d!
	%CONSOLE_DIR%\console.exe /freeimage /threshold defcpu %ITERATIONS% /generate !d! !d! /discard >> defcpu.csv
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolitergpu %ITERATIONS% /generate !d! !d! /discard >> rowcolitergpu.csv
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolnaivegpu %ITERATIONS% /generate !d! !d! /discard >> rowcolnaivegpu.csv
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolbasketgpu %ITERATIONS% /generate !d! !d! /discard >> rowcolbasketgpu.csv
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolefficientgpu %ITERATIONS% /generate !d! !d! /discard >> rowcolefficientgpu.csv
	set /A d=!d!*2
)

ENDLOCAL