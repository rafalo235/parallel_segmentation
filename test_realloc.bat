@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=25
REM -----------------

set d=256
echo %CONSOLE_DIR%
for /l %%x in (1, 1, 30) do (

	set /A y=!d!*%%x-192
	echo Binaryzacja obrazu o rozmiarach !y! na !y!
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolefficientgpu %ITERATIONS% /generate !y! !y! /discard >> testy\sum_no_realloc.csv

	set /A y=!d!*%%x-128
	echo Binaryzacja obrazu o rozmiarach !y! na !y!
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolefficientgpu %ITERATIONS% /generate !y! !y! /discard >> testy\sum_no_realloc.csv

	set /A y=!d!*%%x-64
	echo Binaryzacja obrazu o rozmiarach !y! na !y!
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolefficientgpu %ITERATIONS% /generate !y! !y! /discard >> testy\sum_no_realloc.csv
)

ENDLOCAL