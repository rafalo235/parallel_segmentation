#include "stdafx.h"
//#include <cwchar>
#include <wchar.h>

enum phase_t {
	GET_DECODER,
	GET_JOB,
	GET_XRADIUS,
	GET_YRADIUS,
	GET_PERCENT,
	GET_METHOD,
	GET_BLOCK_WIDTH,
	GET_BLOCK_HEIGHT,
	GET_ITERATIONS,
	GET_INPUT_TYPE,
	GET_OUTPUT_TYPE,
	PRINT_HELP,
	GET_WIDTH,
	GET_HEIGHT,
	GET_INPUT_PATH,
	GET_OUTPUT_PATH,
	FINISH
};

static void printHelp(void);

void parseArguments(int argc, wchar_t* argv[], arguments_t& arguments) {
	phase_t phase = GET_DECODER;

	for (int i = 1; i < argc; ++i) {

		switch (phase) {
		case GET_DECODER:
		{
			if (wcscmp(argv[i], WIC_DECODER_L) == 0) {
				arguments.decoder = WIC_DECODER;
				phase = GET_JOB;
			}
			else if (wcscmp(argv[i], FREEIMAGE_DECODER_L) == 0) {
				arguments.decoder = FREEIMAGE_DECODER;
				phase = GET_JOB;
			}
			else {
				fwprintf(stderr, L"Niepoprawny dekoder %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			break;
		}

		// ----- Job type
		case GET_JOB:
		{
			if (wcscmp(argv[i], DEMO_L) == 0) {
				arguments.job = DEMO;
				phase = GET_METHOD;
			}
			else if (wcscmp(argv[i], THRESHOLD_L) == 0) {
				arguments.job = THRESHOLD;
				phase = GET_XRADIUS;
			}
			else if (wcscmp(argv[i], HELP_L) == 0 || wcscmp(argv[i], HELP_S) == 0) {
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				fwprintf(stderr, L"Niepoprawne zadanie %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			break;
		}

		// ----- Parametry binaryzacji

		case GET_XRADIUS:
		{
			int xRadius = _wtoi(argv[i]);
			if (xRadius == 0) {
				fwprintf(stderr, L"Niepoprawny promien poziomy okna %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				parallel_seg::setXThresholdRadius(xRadius);
				phase = GET_YRADIUS;
			}
			break;
		}

		case GET_YRADIUS:
		{
			int yRadius = _wtoi(argv[i]);
			if (yRadius == 0) {
				fwprintf(stderr, L"Niepoprawny promien pionowy okna %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				parallel_seg::setYThresholdRadius(yRadius);
				phase = GET_PERCENT;
			}
			break;
		}

		case GET_PERCENT:
		{
			int percent = _wtoi(argv[i]);
			if (percent == 0) {
				fwprintf(stderr, L"Niepoprawna wartosc procentowa binaryzacji %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				parallel_seg::setThresholdPercent(percent);
				phase = GET_METHOD;
			}
			break;
		}

		// ----- Method
		case GET_METHOD:
		{
			if (wcscmp(argv[i], DEF_CPU_L) == 0) {
				arguments.method = DEF_CPU;
				arguments.onDevice = false;
				arguments.horizontalBlockSize = arguments.verticalBlockSize = 0;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralDef);
				phase = GET_ITERATIONS;
			}
			else if (wcscmp(argv[i], ROW_COL_ITER_GPU_L) == 0) {
				arguments.method = ROW_COL_ITER_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowCol);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_ITER_T_GPU_L) == 0) {
				arguments.method = ROW_COL_ITER_T_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColT);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_NAIVE_OB_GPU_L) == 0) {
				arguments.method = ROW_COL_NAIVE_OB_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColNaiveOneBlock);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_NAIVE_GPU_L) == 0) {
				arguments.method = ROW_COL_NAIVE_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColNaive);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_BASKET_OB_GPU_L) == 0) {
				arguments.method = ROW_COL_BASKET_OB_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColBasketOneBlock);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_BASKET_GPU_L) == 0) {
				arguments.method = ROW_COL_BASKET_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColBasket);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_EFF_OB_GPU_L) == 0) {
				arguments.method = ROW_COL_EFF_OB_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColEfficientOneBlock);
				phase = GET_BLOCK_WIDTH;
			}
			else if (wcscmp(argv[i], ROW_COL_EFFICIENT_GPU_L) == 0) {
				arguments.method = ROW_COL_EFFICIENT_GPU;
				arguments.onDevice = true;
				parallel_seg::setIntegralFunction(&parallel_seg::imageIntegralRowColEfficient);
				phase = GET_BLOCK_WIDTH;
			}
			else {
				fwprintf(stderr, L"Niepoprawna metoda %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			break;
		}

		// ----- Iterations
		case GET_BLOCK_WIDTH:
		{
			arguments.horizontalBlockSize = _wtoi(argv[i]);
			if (arguments.horizontalBlockSize == 0) {
				fwprintf(stderr, L"Niepoprawna szerokosc bloku %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				parallel_seg::setHorizontalBlockSize(arguments.horizontalBlockSize);
				phase = GET_BLOCK_HEIGHT;
			}
			break;
		}

		// ----- Iterations
		case GET_BLOCK_HEIGHT:
		{
			arguments.verticalBlockSize = _wtoi(argv[i]);
			if (arguments.verticalBlockSize == 0) {
				fwprintf(stderr, L"Niepoprawna szerokosc bloku %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				parallel_seg::setVerticalBlockSize(arguments.verticalBlockSize);
				phase = GET_ITERATIONS;
			}
			break;
		}

		// ----- Iterations
		case GET_ITERATIONS:
		{
			arguments.iterations = _wtoi(argv[i]);
			if (arguments.iterations == 0) {
				fwprintf(stderr, L"Niepoprawna ilosc iteracji %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				phase = GET_INPUT_TYPE;
			}
			break;
		}

		// ----- Input type
		case GET_INPUT_TYPE:
		{
			if (wcscmp(argv[i], GENERATE_DATA_L) == 0) {
				arguments.inputType = GENERATE_DATA;
				phase = GET_WIDTH;
			}
			else if (wcscmp(argv[i], LOAD_L) == 0) {
				arguments.inputType = LOAD;
				phase = GET_INPUT_PATH;
			}
			else{
				fwprintf(stderr, L"Niepoprawny sposob przekazania danych wejsciowych %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			break;
		}
		case GET_WIDTH:
		{
			arguments.width = _wtoi(argv[i]);
			if (arguments.width == 0) {
				fwprintf(stderr, L"Niepoprawna wysokosc %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				phase = GET_HEIGHT;
			}
			break;
		}
		case GET_HEIGHT:
		{
			arguments.height = _wtoi(argv[i]);
			if (arguments.height == 0) {
				fwprintf(stderr, L"Niepoprawna wysokosc %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			else {
				phase = GET_OUTPUT_TYPE;
			}
			break;
		}
		case GET_INPUT_PATH:
		{
			arguments.inputImagePath = argv[i];
			phase = GET_OUTPUT_TYPE;
			break;
		}

		// ----- Output type
		case GET_OUTPUT_TYPE:
		{
			if (wcscmp(argv[i], DISPLAY_L) == 0) {
				arguments.outputType = DISPLAY;
				// TODO argumenty in out
				phase = FINISH;
			}
			else if (wcscmp(argv[i], SAVE_L) == 0) {
				arguments.outputType = SAVE;
				phase = GET_OUTPUT_PATH;
			}
			else if (wcscmp(argv[i], DISCARD_L) == 0) {
				arguments.outputType = DISCARD;
				phase = FINISH;
			}
			else {
				fwprintf(stderr, L"Niepoprawny sposob przekazania danych wyjsciowych %s\r\n", argv[i]);
				arguments.job = HELP;
				printHelp();
				phase = FINISH;
			}
			break;
		}
		case GET_OUTPUT_PATH:
		{
			arguments.outputImagePath = argv[i];
			phase = FINISH;
			break;
		}

		// ----- Help
		case PRINT_HELP:
		{
			arguments.job = HELP;
			printHelp();
			phase = FINISH;
			break;
		}

		}
	}
	if (phase != FINISH) {
		fwprintf(stderr, L"Za malo parametrow\r\n");
		printHelp();
		arguments.job = HELP;
	}

}

void printHelp(void) {
	wprintf(L"Obliczanie obrazu calkowego na karcie graficznej.                     \n");
	wprintf(L"Autor:              Rafal Olejniczak                                  \n");
	printf("Kompilacja:         %s %s                               \r\n", __TIME__, __DATE__);
	wprintf(L"Opcje:                                                                \n");
	wprintf(L" Dekoder:                                                             \n");
	wprintf(L" /wic               Wykorzystana biblioteka Windows Imaging Component \n");
	wprintf(L" /freeimage         Wykorzystana biblioteka FreeImage                 \n");
	wprintf(L"                                                                      \n");
	wprintf(L" Funkcje:                                                             \n");
	wprintf(L" /demo <M> [<W> <H>]obliczanie obrazu calkowego z danych wejsciowych  \n");
	wprintf(L"       <I>          przy pomocy metody <M> (wymienione dalej); dane   \n");
	wprintf(L"                    przetwarzane <I> razy, w celu obliczenia sredniego\n");
	wprintf(L"                    czasu wynkonania; dla algorytmow na karcie grafi- \n");
	wprintf(L"                    cznej nalezy podac wymiary blokow watkow (poziome \n");
	wprintf(L"                    <W> i pionowe <H>                                 \n");
	wprintf(L" /threshold <X> <Y> binaryzacja danych wejsciowych z liczeniem obrazu \n");
	wprintf(L"            <P> <M> calkowego metoda <M>, liczone <I> razy dla usredn-\n");
	wprintf(L"            [<W>    ienia wynikow <X> - poziomy promien okna do binar-\n");
	wprintf(L"            <H>]<I> zacji <Y> - promien w pionie <P> procent odseparo-\n");
	wprintf(L"                    wujacy jasnosc <W> i <H> jak w /demo              \n");
	wprintf(L" /help              pomoc                                             \n");
	wprintf(L"                                                                      \n");
	wprintf(L" Metody <M>:                                                          \n");
	wprintf(L" defcpu             Algorytm definicyjny, liczony sekwencyjnie na CPU \n");
	wprintf(L" rowcolitergpu      Metoda wierszowo kolumnowa, sumujaca w petli      \n");
	wprintf(L" rowcolitertgpu     Metoda wierszowo kolumnowa, sumujaca w petli, usp-\n");
	wprintf(L"                    prawniona przez transpozycje obrazu               \n");
	wprintf(L" rowcolnaiveobgpu   Algorytm naiwny na GPU - jeden blok przetwarzania \n");
	wprintf(L"                    na jeden wiersz/kolumne                           \n");
	wprintf(L" rowcolnaivegpu     Algorytm naiwny na GPU                            \n");
	wprintf(L" rowcolbasketobgpu  Algorytm z podzialem na koszyki na GPU - jeden bl-\n");
	wprintf(L"                    ok przetwarzania na jeden wiersz/kolumne          \n");
	wprintf(L" rowcolbasketgpu    Algorytm z podzialem na koszyki na GPU            \n");
	wprintf(L" rowcoleffobgpu     Algorytm efektywny na GPU - jeden blok przetwarza-\n");
	wprintf(L"                    nia na jeden wiersz/kolumne                       \n");
	wprintf(L" rowcolefficientgpu Algorytm efektywny na GPU                         \n");
	wprintf(L"                                                                      \n");
	wprintf(L" Dane wejsciowe:                                                      \n");
	wprintf(L" /generate <W> <H>  generuj dane rosnace o szerokosci <W> i wysokosci \n");
	wprintf(L"                    <H>                                               \n");
	wprintf(L" /input <P>         zaladuj obraz zapisany w sciezce <P>              \n");
	wprintf(L"                                                                      \n");
	wprintf(L" Dane wyjsciowe:                                                      \n");
	wprintf(L" /display           wydrukuj rezultaty algorytmu                      \n");
	wprintf(L" /output <P>        zapisz rezultaty w postaci obrazu pod sciezka <P> \n");
	wprintf(L" /discard           wypisz tylko czas wykonywania obliczen            \n");
	wprintf(L"                                                                      \n");
	printf(parallel_seg::FreeImageDecoder::getCopyright());
}