// console.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace parallel_seg;

int _tmain(int argc, wchar_t* argv[]) {
	arguments_t arguments;
	//parallel_seg::FreeImageDecoder decoder;
	parallel_seg::IDecoder *decoder = nullptr;
	Image3c *img = nullptr, *img2 = nullptr;
	float *data = nullptr, *data2 = nullptr;
	double sumTime = 0.0;
	double allTime = 0.0;
	
	parseArguments(argc, argv, arguments);

	if (arguments.job == job_t::HELP)
		return 0;

	if (arguments.decoder == decoder_t::FREEIMAGE_DECODER)
		decoder = new FreeImageDecoder();
	else if (arguments.decoder == decoder_t::WIC_DECODER)
		decoder = new WICDecoder();
	
	decoder->initialize();
	init();

	if (arguments.job == DEMO) {
		// Input data
		if (arguments.inputType == inputType_t::GENERATE_DATA) {
			data = generateFloatSequence(arguments.width, arguments.height, arguments.onDevice);
			data2 = generateFloat(arguments.width + 1, arguments.height + 1, arguments.onDevice);
		}
		else if (arguments.inputType == inputType_t::LOAD) {
			img = decoder->decode(arguments.inputImagePath, arguments.onDevice);
			arguments.width = img->width; arguments.height = img->height;
			data = generateFloat(img->width, img->height, arguments.onDevice);
			getImageLuminance(img->pixels, data, img->width, img->height, arguments.onDevice);
			data2 = generateFloat(img->width + 1, img->height + 1, arguments.onDevice);
		}

		// Test function
		for (int i = 0; i < arguments.iterations; i++) sumTime += (getIntegralFunction())(data, &data2, arguments.width, arguments.height);
		sumTime /= arguments.iterations;

		// Output data
		wprintf(L"%d\t%d\t%d\t%d\t%f\n", arguments.width, arguments.height, arguments.horizontalBlockSize, arguments.verticalBlockSize, sumTime);
		if (arguments.outputType == outputType_t::SAVE) {
			saveFloatMatrix(data2, arguments.width + 1, arguments.height + 1, arguments.outputImagePath, arguments.onDevice);
		}
		else if (arguments.outputType == outputType_t::DISPLAY) {
			printFloatMatrix(data2, arguments.width + 1, arguments.height + 1, arguments.onDevice);
		} // else DISCARD
	}

	else if (arguments.job == THRESHOLD) {
		// Input data
		if (arguments.inputType == inputType_t::GENERATE_DATA) {
			// Dane wej�ciowe do binaryzacji zawsze w pami�ci operacyjnej CPU (wsp�lny punkt wej�ciowy dla algorytm�w)
			img = generateImage(arguments.width, arguments.height, false);
		}
		else if (arguments.inputType == inputType_t::LOAD) {
			img = decoder->decode(arguments.inputImagePath, false);
			arguments.width = img->width; arguments.height = img->height;
		}
		img2 = generateImage(arguments.width, arguments.height, false);

		start();
		if (arguments.onDevice)
			for (int i = 0; i < arguments.iterations; i++) sumTime += gpuAdaptiveThreasholding(img->pixels, img2->pixels, img->width, img->height);
		else
			for (int i = 0; i < arguments.iterations; i++) sumTime += gpucpuAdaptiveThresholding(img->pixels, img2->pixels, img->width, img->height);
		allTime = end() / arguments.iterations;
		sumTime /= arguments.iterations;

		// Output data
		wprintf(L"%d\t%d\t%d\t%d\t%f\t%f\n", arguments.width, arguments.height, arguments.horizontalBlockSize, arguments.verticalBlockSize, allTime, sumTime);
		if (arguments.outputType == outputType_t::SAVE) {
			decoder->encode(*img2, arguments.outputImagePath);
		}
		else if (arguments.outputType == outputType_t::DISPLAY) {
			printPixelMatrix(img2->pixels, arguments.width, arguments.height, false);
		} // else DISCARD
	}

	decoder->uninitialize();
	
	if (data != nullptr) parallel_seg::release(data, arguments.onDevice);
	if (data2 != nullptr) parallel_seg::release(data2, arguments.onDevice);
	if (img != nullptr) delete img;
	if (img2 != nullptr) delete img2;

	parallel_seg::finish();

	return 0;
}

