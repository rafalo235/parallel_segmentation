#include "stdafx.h"

#include <Windows.h>

static LARGE_INTEGER __frequency, __t1, __t2;

void init(void) {
	QueryPerformanceFrequency(&__frequency);
}

void start(void) {
	QueryPerformanceCounter(&__t1);
}

double end(void) {
	QueryPerformanceCounter(&__t2);
	return (__t2.QuadPart - __t1.QuadPart) * 1000.0 / __frequency.QuadPart;
}