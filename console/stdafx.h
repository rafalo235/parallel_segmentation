// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"

#include <tchar.h>
#include <iostream>

#include "core.h"
#include "IDecoder.h"
#include "FreeImageDecoder.h"
#include "WICDecoder.h"
#include "arguments.h"
#include "measurement.h"
