#pragma once

enum decoder_t {
	WIC_DECODER,
	FREEIMAGE_DECODER
};
#define WIC_DECODER_L			L"/wic"
#define FREEIMAGE_DECODER_L		L"/freeimage"

enum job_t {
	DEMO,
	THRESHOLD,
	HELP
};
#define DEMO_L			L"/demo"
#define THRESHOLD_L		L"/threshold"
#define HELP_L			L"/help"
#define HELP_S			L"/h"

enum method_t {
	DEF_CPU,
	ROW_COL_ITER_GPU,
	ROW_COL_ITER_T_GPU,
	ROW_COL_NAIVE_OB_GPU,
	ROW_COL_NAIVE_GPU,
	ROW_COL_BASKET_OB_GPU,
	ROW_COL_BASKET_GPU,
	ROW_COL_EFF_OB_GPU,
	ROW_COL_EFFICIENT_GPU
};
#define DEF_CPU_L				L"defcpu"
#define ROW_COL_ITER_GPU_L		L"rowcolitergpu"
#define ROW_COL_ITER_T_GPU_L	L"rowcolitertgpu"
#define ROW_COL_NAIVE_OB_GPU_L	L"rowcolnaiveobgpu"
#define ROW_COL_NAIVE_GPU_L		L"rowcolnaivegpu"
#define ROW_COL_BASKET_OB_GPU_L	L"rowcolbasketobgpu"
#define ROW_COL_BASKET_GPU_L	L"rowcolbasketgpu"
#define ROW_COL_EFF_OB_GPU_L	L"rowcoleffobgpu"
#define ROW_COL_EFFICIENT_GPU_L	L"rowcolefficientgpu"

enum inputType_t {
	GENERATE_DATA,
	LOAD
};
#define GENERATE_DATA_L	L"/generate"
#define LOAD_L			L"/input"

enum outputType_t {
	DISPLAY,
	SAVE,
	DISCARD
};
#define	DISPLAY_L		L"/display"
#define SAVE_L			L"/output"
#define DISCARD_L		L"/discard"

struct arguments_t {
	decoder_t decoder;
	job_t job;
	method_t method;
	inputType_t inputType;
	outputType_t outputType;
	parallel_seg::integralFunction_t integralFunction;
	int iterations;
	wchar_t *inputImagePath;
	wchar_t *outputImagePath;
	int width;
	int height;
	int horizontalBlockSize;
	int verticalBlockSize;
	bool onDevice;
};

void parseArguments(int argc, wchar_t* argv[], arguments_t& arguments);
