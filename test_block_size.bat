@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=20
REM -----------------

set d=128
set s=1024
echo %CONSOLE_DIR%
for /l %%y in (1, 1, 25) do (
	echo Obraz !s! na !s!
	for /l %%x in (1, 1, 13) do (

		echo Liczenie obrazu calkowego w blokiach !d! kolumny na !d! wiersze 
		%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolitergpu !d! !d! %ITERATIONS% /generate !s! !s! /discard >> testy\block_size\sum_block_size.csv
		set /A d=!d!+32
	)
	set /A d=128
	set /A s=!s!+128
)

ENDLOCAL