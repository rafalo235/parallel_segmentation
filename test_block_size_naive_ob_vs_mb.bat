@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=20
REM -----------------

set d=128
echo %CONSOLE_DIR%
for /l %%x in (1, 1, 13) do (

	echo Liczenie obrazu calkowego w blokiach !d! kolumny na !d! wiersze 
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolnaivegpu !d! !d! %ITERATIONS% /generate 2048 2048 /discard >> testy\naive_ob_vs_mb\block_size_mb.csv
	
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolnaiveobgpu !d! !d! %ITERATIONS% /generate 2048 2048 /discard >> testy\naive_ob_vs_mb\block_size_ob.csv
	
	set /A d=!d!+32
)


ENDLOCAL