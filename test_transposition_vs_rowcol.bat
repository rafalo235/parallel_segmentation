@echo off

REM Badanie binaryzacji obrazu
SETLOCAL ENABLEDELAYEDEXPANSION

REM Parametry skryptu
REM -----------------
set CONSOLE_DIR=.\Release
set ITERATIONS=40
REM -----------------

set s=128
echo %CONSOLE_DIR%
for /l %%y in (1, 1, 32) do (
	
	echo Obraz !s! na !s!
	
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolitergpu 256 256 %ITERATIONS% /generate !s! !s! /discard >> testy\transposition\sum_rowcoliter.csv
	
	%CONSOLE_DIR%\console.exe /freeimage /threshold rowcolitertgpu 256 256 %ITERATIONS% /generate !s! !s! /discard >> testy\transposition\sum_rowcoliter_transpose.csv

	set /A s=!s!+128
)

ENDLOCAL