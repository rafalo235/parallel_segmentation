#include "core.h"

#include "Types.h"
#include "GPUFunctions.h"
#include "CPUFunctions.h"

namespace parallel_seg {

	extern integralFunction_t _integralFunction;
	extern int xThresholdRadius;
	extern int yThresholdRadius;
	extern int thresholdPercent;
	
	__global__ void adaptiveThresholdingFGPU(const float *in, const float *integral, pixel3c *out, int width, int height, int xWindow, int yWindow, int percent);

	double gpuAdaptiveThreasholding(const pixel3c *in, pixel3c *out, int width, int height) {
		float *luminance, *integral;
		pixel3c *inC, *outC;
		dim3 grid, block;
		int size = width * height;
		int integralSize = (width + 1) * (height + 1);
		
		// Kopia obrazu na karcie
		if (cudaMalloc((void**)&inC, size * sizeof(pixel3c)) != cudaSuccess) 
			throw std::bad_alloc();
		if (cudaMemcpy(inC, in, size * sizeof(pixel3c), cudaMemcpyHostToDevice) != cudaSuccess)
			throw std::exception("Nie mozna zaladowac obrazu w pixel3c do pamieci karty");

		// Konwersja na jasnosc
		if (cudaMalloc((void**)&luminance, size * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		convertToLuminance(inC, luminance, width, height);
		cudaFree(inC);

		// Obliczenie obrazu ca�kowego
		if (cudaMalloc((void**)&integral, integralSize * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		double elapsedTime = _integralFunction(luminance, &integral, width, height);
		
		// Binaryzacja obrazu
		if (cudaMalloc((void**)&outC, size * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		block.x = 16; block.y = 16;
		grid.x = CEIL(width, block.x); grid.y = CEIL(height, block.y);
		adaptiveThresholdingFGPU<<<grid, block>>>(luminance, integral, outC, width, height, xThresholdRadius, yThresholdRadius, thresholdPercent);
		cudaFree(luminance);
		cudaFree(integral);

		// Przeniesienie do pamieci operacyjnej
		if (cudaMemcpy(out, outC, size * sizeof(pixel3c), cudaMemcpyDeviceToHost) != cudaSuccess) 
			throw std::exception("Nie mozna przeniesc danych do bufora wyjsciowego");
		cudaFree(outC);

		return elapsedTime;
	}

	double gpucpuAdaptiveThresholding(const pixel3c *in, pixel3c *out, int width, int height) {
		int size = width * height;
		int widthIntegral = width + 1,
			heightIntegral = height + 1;
		int sizeIntegral = widthIntegral * heightIntegral;
		float *luminanceC, *integralC, *luminance, *integral;
		pixel3c *outC;
		dim3 grid, block;
		
		//  Jasno�� obrazu
		if ((luminance = (float*)malloc(size * sizeof(float))) == nullptr)
			throw std::bad_alloc();
		cpuGetLuminance(in, luminance, size);

		// Obraz ca�kowy
		if ((integral = (float*)calloc(sizeIntegral, sizeof(float))) == nullptr)
			throw std::bad_alloc();
		double elapsedTime = imageIntegralDef(luminance, &integral, width, height);
		
		// Binaryzacja obrazu
		if (cudaMalloc((void**)&luminanceC, size * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		if (cudaMalloc((void**)&integralC, sizeIntegral * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		if (cudaMalloc((void**)&outC, size * sizeof(pixel3c)) != cudaSuccess)
			throw std::bad_alloc();
		if (cudaMemcpy(luminanceC, luminance, size * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess)
			throw std::exception("Nie mozna przekopiowac jasnosci na karte przed binaryzacja");
		if (cudaMemcpy(integralC, integral, sizeIntegral * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess)
			throw std::exception("Nie mozna przekopiowac obrazu calkowego na karte przed binaryzacja");
		free(luminance);
		free(integral);
		block.x = 16; block.y = 16;
		grid.x = CEIL(width, block.x); grid.y = CEIL(height, block.y);
		adaptiveThresholdingFGPU<<<grid, block>>>(luminanceC, integralC, outC, width, height, xThresholdRadius, yThresholdRadius, thresholdPercent);
		cudaFree(luminanceC);
		cudaFree(integralC);

		// Przeniesienie do pami�ci operacyjnej
		if (cudaMemcpy(out, outC, size * sizeof(pixel3c), cudaMemcpyDeviceToHost) != cudaSuccess)
			throw std::exception("Nie mozna przekopiowac wynikow binaryzacji do pamieci CPU");
		cudaFree(outC);

		return elapsedTime;
	}

	__global__ void adaptiveThresholdingFGPU(const float *in, const float *integral, pixel3c *out, int width, int height, int xWindow, int yWindow, int percent) {
		int x = blockDim.x * blockIdx.x + threadIdx.x;
		int y = blockDim.y * blockIdx.y + threadIdx.y;

		if (x < width && y < height) {
			int widthIntegral = width + 1;

			int r = x + 1 + xWindow;
			int b = y + 1 + yWindow;
			int l = x + 1 - xWindow;
			int t = y + 1 - yWindow;

			if (r > width)	r = width;
			if (b > height)	b = height;
			if (l < 0)		l = 0;
			if (t < 0)		t = 0;

			int windowSize = (r - l) * (b - t);
			float sum = integral[b * widthIntegral + r] + integral[t * widthIntegral + l]
				- integral[b * widthIntegral + l] - integral[t * widthIntegral + r];
			
			if (in[y * width + x] * (float)windowSize < (sum * (float)(100 - percent)) / 100.f)
				out[y * width + x] = pixel3c(0.f);
			else
				out[y * width + x] = pixel3c(255.f);

		}
	}

}