#include "GPUFunctions.h"
#include "Types.h"

#define ROWS_BLOCK_WIDTH 32
#define ROWS_BLOCK_HEIGHT 8

#define COLS_BLOCK_WIDTH 8
#define COLS_BLOCK_HEIGHT 32

namespace parallel_seg {

	__global__ void imageAddLastElementsToRows(const float *in, float *out,
		int widthOut, int heightOut, int horizontalBlock);

	void addHorizontalLastElements(const float *in, float *out, int widthOut, int heightOut, int horizontalBlock) {
		dim3 block(ROWS_BLOCK_WIDTH, ROWS_BLOCK_HEIGHT), grid;
		grid.x = CEIL(widthOut, horizontalBlock); grid.y = CEIL(heightOut, ROWS_BLOCK_HEIGHT);
		imageAddLastElementsToRows<<<grid, block>>>(in, out, widthOut, heightOut, horizontalBlock);
	}

	__global__ void imageAddLastElementsToRows(const float *in, float *out,
			int widthOut, int heightOut, int horizontalBlock) {

		int xOut = horizontalBlock * blockIdx.x + threadIdx.x;
		const int end = horizontalBlock * blockIdx.x + horizontalBlock < widthOut ? 
			horizontalBlock * blockIdx.x + horizontalBlock :
			widthOut;
		const int yOut = blockDim.y * blockIdx.y + threadIdx.y;

		if (yOut < heightOut) {
			const float value = in[(gridDim.x + 1) * yOut + blockIdx.x];
			for (; xOut < end; xOut += blockDim.x) {
				out[yOut * (widthOut + 1) + xOut] += value;
			}
		}
	}

	__global__ void imageAddLastElementsToCols(const float *in, float *out,
		int widthOut, int heightOut, int verticalBlock);

	void addVerticalLastElements(const float *in, float *out, int widthOut, int heightOut, int verticalBlock) {
		dim3 block(COLS_BLOCK_WIDTH, COLS_BLOCK_HEIGHT), grid;
		grid.x = CEIL(widthOut, COLS_BLOCK_WIDTH); grid.y = CEIL(heightOut, verticalBlock);
		imageAddLastElementsToCols<<<grid, block>>>(in, out, widthOut, heightOut, verticalBlock);
	}

	__global__ void imageAddLastElementsToCols(const float *in, float *out,
			int widthOut, int heightOut, int verticalBlock) {

		const int xOut = blockDim.x * blockIdx.x + threadIdx.x;
		int yOut = verticalBlock * blockIdx.y + threadIdx.y;
		const int end = verticalBlock * blockIdx.y + verticalBlock < heightOut ?
			verticalBlock * blockIdx.y + verticalBlock :
			heightOut;

		if (xOut < widthOut) {
			const float value = in[(gridDim.y + 1) * xOut + blockIdx.y];
			for (; yOut < end; yOut += blockDim.y) {
				out[yOut * (widthOut + 1) + xOut] += value;
			}
		}
	}

}