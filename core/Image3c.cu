#include "IDecoder.h"

#include "GPUFunctions.h"
#include <cuda.h>
#include "Types.h"

namespace parallel_seg {

	Image3c::~Image3c() {
		if (pixels != nullptr) {
			if (this->onDevice)
				cudaFree(this->pixels);
			else
				free(this->pixels);
		}
	}

	Image3c* Image3c::copy(bool toDevice) const {
		Image3c* ret = new Image3c();
		ret->height = height;
		ret->width = width;
		ret->size = size;
		ret->onDevice = toDevice;
		if (toDevice) {
			if (cudaMalloc((void**)&ret->pixels, size) != cudaSuccess)
				throw "Cannot allocate memory on device";

			if (cudaMemcpy(ret->pixels, pixels, size, onDevice ? cudaMemcpyDeviceToDevice : cudaMemcpyHostToDevice) != cudaSuccess)
				throw "Cannot copy memory to device";
		}
		else {
			ret->pixels = (pixel3c*) new unsigned char[size];

			if (cudaMemcpy(ret->pixels, pixels, size, onDevice ? cudaMemcpyDeviceToHost : cudaMemcpyHostToHost) != cudaSuccess)
				throw "Cannot copy memory to host";
		}
		return ret;
	}

}