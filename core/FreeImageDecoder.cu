#include "FreeImageDecoder.h"

#include "FreeImage.h"
#include <cstdlib>
#include "Types.h"
#include <cuda.h>
#include <exception>

namespace parallel_seg {

	void FreeImageDecoder::initialize(void) {
		FreeImage_Initialise();
	}

	void FreeImageDecoder::uninitialize(void) {
		FreeImage_DeInitialise();
	}

	Image3c *FreeImageDecoder::decode(wchar_t *path, bool toDevice) {
		Image3c *image = new Image3c();
		//FREE_IMAGE_FORMAT fif = FreeImage_GetFileTypeU(path);
		//if (fif == FIF_UNKNOWN) throw "Unknown format";
		FIBITMAP *bitmapHandle = FreeImage_LoadU(FIF_JPEG, path, JPEG_ACCURATE);
		if (!bitmapHandle)
			throw std::bad_alloc();
		FIBITMAP *bmp2 = FreeImage_ConvertTo24Bits(bitmapHandle);
		if (!bmp2)
			throw std::bad_alloc();
		FreeImage_Unload(bitmapHandle);

		image->width = FreeImage_GetWidth(bmp2);
		image->height = FreeImage_GetHeight(bmp2);
		image->onDevice = toDevice;
		image->size = image->width * image->height * sizeof(pixel3c);
		if (toDevice) {
			if (cudaMalloc((void**)&image->pixels, image->size) != cudaSuccess) 
				throw std::bad_alloc();
			if (cudaMemcpy(image->pixels, FreeImage_GetBits(bmp2), image->size, cudaMemcpyHostToDevice) != cudaSuccess)
				throw std::exception("Nie mozna zaladowac obrazu do pamieci karty");
		}
		else {
			if ((image->pixels = (pixel3c*)malloc(image->size)) == nullptr) 
				throw std::bad_alloc();
			if (cudaMemcpy(image->pixels, FreeImage_GetBits(bmp2), image->size, cudaMemcpyHostToHost) != cudaSuccess)
				throw std::exception("Nie mozna zaladowac obrazu do pamieci CPU");
		}
		FreeImage_Unload(bmp2);

		return image;
	}

	void FreeImageDecoder::encode(const Image3c& image, wchar_t *path) {
		FIBITMAP *bitmap = FreeImage_AllocateT(FIT_BITMAP, image.width, image.height, 24, 0x00FF0000u, 0x0000FF00u, 0x000000FFu);
		if (!bitmap)
			throw std::bad_alloc();

		if (image.onDevice) {
			if (cudaMemcpy(FreeImage_GetBits(bitmap), image.pixels, image.size, cudaMemcpyDeviceToHost) != cudaSuccess)
				throw std::exception("Blad przy kopiowaniu danych do zapisu");
		}
		else {
			if (cudaMemcpy(FreeImage_GetBits(bitmap), image.pixels, image.size, cudaMemcpyHostToHost) != cudaSuccess)
				throw std::exception("Blad przy kopiowaniu danych do zapisu");
		}

		FreeImage_SaveU(FIF_JPEG, bitmap, path, JPEG_QUALITYSUPERB);
		FreeImage_Unload(bitmap);
	}

	const char* FreeImageDecoder::getCopyright(void) {
		return FreeImage_GetCopyrightMessage();
	}

}