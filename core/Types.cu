
#include "IDecoder.h"
#include "Types.h"

namespace parallel_seg {

	HOST_DEVICE pixel3c pixel3c::xyzToRgb(pixel3f xyz) {
		float r, g, b;
		xyz.x /= 100.f;
		xyz.y /= 100.f;
		xyz.z /= 100.f;

		r = xyz.x *  3.2406f + xyz.y * -1.5372f + xyz.z * -0.4986f;
		g = xyz.x * -0.9689f + xyz.y *  1.8758f + xyz.z *  0.0415f;
		b = xyz.x *  0.0557f + xyz.y * -0.2040f + xyz.z *  1.0570f;

		if (r > 0.0031308f)		r = 1.055f * powf(r, 1.f / 2.4f) - 0.055f;
		else					r = 12.92f * r;
		if (g > 0.0031308f)		g = 1.055f * powf(g, 1.f / 2.4f) - 0.055f;
		else					g = 12.92f * g;
		if (b > 0.0031308f)		b = 1.055f * powf(b, 1.f / 2.4f) - 0.055f;
		else					b = 12.92f * b;

		return pixel3c(
			(channel)(r * 255.f),
			(channel)(g * 255.f),
			(channel)(b * 255.f)
			);
	}

	HOST_DEVICE pixel3f pixel3f::rgbToXyz(const pixel3c& rgb) {
		float r = (float)rgb.values[0] / MAX_COLOR_VAL;
		float g = (float)rgb.values[1] / MAX_COLOR_VAL;
		float b = (float)rgb.values[2] / MAX_COLOR_VAL;

		if (r > 0.04045f)		r = powf(((r + 0.055f) / 1.055f), 2.4f);
		else					r = r / 12.92f;
		if (g > 0.04045f)		g = powf(((g + 0.055f) / 1.055f), 2.4f);
		else					g = g / 12.92f;
		if (b > 0.04045f)		b = powf(((b + 0.055f) / 1.055f), 2.4f);
		else					b = b / 12.92f;

		r *= 100.f; g *= 100.f; b *= 100.f;

		return pixel3f(
			r * 0.4124f + g * 0.3576f + b * 0.1805f,
			r * 0.2126f + g * 0.7152f + b * 0.0722f,
			r * 0.0193f + g * 0.1192f + b * 0.9505f
			);
	}

	HOST_DEVICE pixel3f pixel3f::xyzToHunterLab(const pixel3f& xyz) {
		return pixel3f(
			10.f * sqrtf(xyz.y),
			17.5f * (((1.02f * xyz.x) - xyz.y) / sqrtf(xyz.y)),
			7.f * ((xyz.y - (0.847f * xyz.z)) / sqrtf(xyz.y))
			);
	}

	HOST_DEVICE pixel3f pixel3f::hunterLabToXyz(const pixel3f& lab) {
		pixel3f xyz;
		float y = lab.l / 10.f;
		float x = lab.a / 17.5f * lab.l / 10.f;
		float z = lab.b / 7.f * lab.l / 10.f;

		xyz.y = powf(y, 2.f);
		xyz.x = (x + xyz.y) / 1.02f;
		xyz.z = -(z - xyz.y) / 0.847f;

		return xyz;
	}

	HOST_DEVICE double pixel3f::difference(const pixel3f &a, const pixel3f &b) {
		pixel3f c = operator-(a, b);
		return sqrt((double)(c.values[0] * c.values[0] + c.values[1] * c.values[1] + c.values[2] * c.values[2]));
	}

}