#pragma once

#include "Types.h"


namespace parallel_seg {
	// Forward declaration
	class Histogram;
	class LuminanceHistogram;
	class ColorCreator;

	// Segmentacja
	void cpuCreateLuminanceHistogram(const pixel3c *in, unsigned int size, LuminanceHistogram &histogram);
	
	void cpuSplittingAndMerging(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int wend, unsigned int hend, unsigned int stride, const ColorCreator& creator);
	void cpuRegionGrowing(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int x, unsigned int y);
	void indexSequentailly(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int sx, unsigned int sy);

	void splitImage(const pixel3f *pixelsLab, pixel3f *out, int width, int height);

	// Obraz ca�kowy
	double imageIntegralDef(const pixel3f *in, pixel3f *out, int width, int height);
	
	void cpuGetLuminance(const pixel3c *in, float *out, int size);
	
	int* derivate(const int* data, unsigned int length);
	void filterMean(int* data, unsigned int length, unsigned short windowLength);
	void filterMedian(int* data, unsigned int length, unsigned short windowRadius);
	int* getZeroPoints(const int *data, unsigned int length, int &counter);
	int* getIndices(unsigned int length);

	void saveCSVData(const wchar_t* path, const int** data, unsigned int seriesNumber, unsigned int length);
	void printMatrix(const pixel3f* matrix, int width, int height);

}