#pragma once

#ifdef PARALLEL_SEGMENTATION_DLL_EXPORTS
#include <cuda_runtime_api.h>
#define HOST_DEVICE __host__ __device__
#define DEVICE __device__
#define GLOBAL __global__
#else
#include "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/include/vector_types.h"
#define HOST_DEVICE
#define DEVICE
#define GLOBAL
#endif

#include <math.h>
#include <cstdint>
#include <stdexcept>

namespace parallel_seg {

	typedef unsigned char channel;
	const channel MAX_COLOR_VAL = 0xFF;
	const double THRESHOLD = 57.5;

	struct pixel3c;
	union pixel3f;

	struct pixel3c {
		channel values[3];

		HOST_DEVICE pixel3c() { }
		HOST_DEVICE pixel3c(const channel &r, const channel &g, const channel &b) {
			values[0] = r; values[1] = g; values[2] = b;
		}
		HOST_DEVICE pixel3c(float gray) {
			values[0] = values[1] = values[2] = (channel)gray;
		}
		HOST_DEVICE pixel3c(const pixel3c &pix) {
			*this = pix;
		}
		HOST_DEVICE pixel3c(const int3& value) {
			values[0] = value.x; values[1] = value.y; values[2] = value.z;
		}

		HOST_DEVICE channel operator[](int index) const {
			return this->values[index];
		}
		HOST_DEVICE channel& operator[](int index) {
			return this->values[index];
		}

		HOST_DEVICE pixel3c& operator=(const pixel3c& value) {
			values[0] = value.values[0]; values[1] = value.values[1]; values[2] = value.values[2];
			return *this;
		}

		bool operator==(const pixel3c& value) const {
			return values[0] == value.values[0] &&
				values[1] == value.values[1] &&
				values[2] == value.values[2];
		}

		bool operator!=(const pixel3c& value) const {
			return values[0] != value.values[0] ||
				values[1] != value.values[1] ||
				values[2] != value.values[2];
		}

		bool operator < (const pixel3c& arg) const {
			return !(values[2] >= arg.values[2])
				&& !(values[1] >= arg.values[1])
				&& !(values[0] >= arg.values[0]);
		}

		bool operator > (const pixel3c& arg) const {
			return !(values[2] <= arg.values[2])
				&& !(values[1] <= arg.values[1])
				&& !(values[0] <= arg.values[0]);
		}

		operator uint32_t() const {
			return (*((uint16_t*)values) | (values[2] << 16)) & 0x00FFFFFF;
		}
		HOST_DEVICE operator int3() const {
			int3 value;
			value.x = values[0]; value.y = values[1]; value.z = values[2];
			return value;
		}

		DEVICE pixel3c& negative() {
			values[0] = MAX_COLOR_VAL - values[0];
			values[1] = MAX_COLOR_VAL - values[1];
			values[2] = MAX_COLOR_VAL - values[2];
			return *this;
		}

		HOST_DEVICE friend pixel3c operator+(const pixel3c &a, const pixel3c &b) {
			return pixel3c(a[0] + b[0], a[1] + b[1], a[2] + b[2]);
		}
		HOST_DEVICE friend pixel3c operator-(const pixel3c &a, const pixel3c &b) {
			return pixel3c(a[0] - b[0], a[1] - b[1], a[2] - b[2]);
		}
		friend pixel3c operator*(const pixel3c &a, const pixel3c &b) {
			return pixel3c(a[0] * b[0], a[1] * b[1], a[2] * b[2]);
		}
		HOST_DEVICE friend pixel3c operator*(const pixel3c &a, const int &b) {
			return pixel3c(a[0] * b, a[1] * b, a[2] * b);
		}
		friend pixel3c operator/(const pixel3c &a, const pixel3c &b) {
			return pixel3c(a[0] / b[0], a[1] / b[1], a[2] / b[2]);
		}
		friend pixel3c operator/(const pixel3c &a, const int &b) {
			return pixel3c(a.values[0] / b, a.values[1] / b, a.values[2] / b);
		}
		static pixel3c divide(const uint3& a, const unsigned int &n) {
			return pixel3c(a.x / n, a.y / n, a.z / n);
		}

		HOST_DEVICE static pixel3c geometricMean(const pixel3c &a, const pixel3c &b) {
			return pixel3c(
				sqrtf(float(a[0]) * float(b[0])),
				sqrtf(float(a[1]) * float(b[1])),
				sqrtf(float(a[2]) * float(b[2]))
				);
		}

		HOST_DEVICE float luminance() const {
			return 0.2126f*values[0] + 0.7152f*values[1] + 0.0722f*values[2];
		}

		static double difference(const pixel3c& a, const pixel3c& b) {
			pixel3c c = a - b;
			return sqrt((double)(c.values[0] * c.values[0] + c.values[1] * c.values[1] + c.values[2] * c.values[2]));

			/*return sqrt(double((a.values[0] - b.values[0]) * (a.values[0] - b.values[0]) +
				(a.values[1] - b.values[1]) * (a.values[1] - b.values[1]) +
				(a.values[2] - b.values[2]) * (a.values[2] - b.values[2]))); */
		}
		
		HOST_DEVICE static pixel3c xyzToRgb(pixel3f xyz);

	};

	const pixel3c BLACK(0, 0, 0);
	const pixel3c WHITE(MAX_COLOR_VAL, MAX_COLOR_VAL, MAX_COLOR_VAL);

	struct pixel4c {
		channel values[4];
	};

	struct sample {
		pixel3c* pixel = nullptr;
		unsigned int x, y;
		bool checked = false;

		sample() {
			x = 0;
			y = 0;
		}

		sample(pixel3c *pixel, unsigned int x, unsigned int y) {
			this->pixel = pixel;
			this->x = x;
			this->y = y;
			checked = false;
		}
	};

#define HUNTER_LAB_THRESHOLD		(20.f)
	
	union pixel3f {
		struct { float x, y, z; };
		struct { float l, a, b; };
		float values[3];

		HOST_DEVICE pixel3f() {
			values[0] = values[1] = values[2] = 0.f;
		}
		HOST_DEVICE pixel3f(float p0, float p1, float p2) {
			values[0] = p0; values[1] = p1; values[2] = p2;
		}
		HOST_DEVICE pixel3f(const pixel3f &pix) {
			*this = pix;
		}
		HOST_DEVICE pixel3f& operator=(const pixel3f& value) {
			values[0] = value.values[0]; values[1] = value.values[1]; values[2] = value.values[2];
			return *this;
		}
		HOST_DEVICE pixel3f& operator+=(const pixel3f& a) {
			x += a.x; y += a.y; z += a.z;
			return *this;
		}
		HOST_DEVICE pixel3f& operator-=(const pixel3f& a) {
			x -= a.x; y -= a.y; z -= a.z;
			return *this;
		}
		HOST_DEVICE pixel3f& operator/=(float a) {
			x /= a; y /= a; z /= a;
			return *this;
		}

		HOST_DEVICE bool operator==(const pixel3f &a) const {
			return values[0] == a.values[0]
				&& values[1] == a.values[1]
				&& values[2] == a.values[2];
		}
		HOST_DEVICE bool operator!=(const pixel3f &a) const {
			return values[0] != a.values[0]
				|| values[1] != a.values[1]
				|| values[2] != a.values[2];
		}
		HOST_DEVICE friend bool operator<(const pixel3f &a, const pixel3f &b) {
			return a.x < b.x && a.y < b.y && a.z < b.z;
		}

		HOST_DEVICE friend pixel3f operator+(const pixel3f &a, const pixel3f &b) {
			return pixel3f(a.x + b.x, a.y + b.y, a.z + b.z);
		}
		HOST_DEVICE friend pixel3f operator-(const pixel3f &a, const pixel3f &b) {
			return pixel3f(a.x - b.x, a.y - b.y, a.z - b.z);
		}
		HOST_DEVICE friend pixel3f operator*(const pixel3f &a, const int &b) {
			float bf = (float)b;
			return pixel3f(a.x * bf, a.y * bf, a.z * bf);
		}
		HOST_DEVICE friend pixel3f operator/(const pixel3f &a, const int &b) {
			float bf = (float)b;
			return pixel3f(a.x / bf, a.y / bf, a.z / bf);
		}
		HOST_DEVICE static double difference(const pixel3f &a, const pixel3f &b);

		HOST_DEVICE static pixel3f rgbToXyz(const pixel3c& rgb);
		HOST_DEVICE static pixel3f xyzToHunterLab(const pixel3f& xyz);
		HOST_DEVICE static pixel3f hunterLabToXyz(const pixel3f& xyz);

	};

	const pixel3f BLACK_LAB(0.f, 0.f, 0.f);
	const pixel3f WHITE_LAB(100.f, -5.336f, 5.433f);

	struct sortedTable_t {
	private:
		int* values;
		int length;
		int elements = 0;
	public:
		sortedTable_t(int count) {
			this->length = count;
			this->values = new int[count];
		}
		~sortedTable_t() {
			free(this->values);
		}
		void add(int value) {
			if (elements == length)
				throw std::length_error("Not enough space for value " + value);
			else {
				int i = elements - 1;
				while (i >= 0 && values[i] > value) {
					values[i + 1] = values[i];
					i--;
				}
				values[i + 1] = value;
				elements++;
			}
		}
		void remove(int value) {
			int i;
			for (i = 0; i < elements; i++) {
				if (values[i] == value) {
					elements--;
					break;
				}
			}
			for (; i < elements; i++) {
				values[i] = values[i + 1];
			}
		}
		int operator [](int i) const { return values[i]; }
		int& operator [](int i) { return values[i]; }
		int median() const {
			if (elements == 0)
				throw std::length_error("No elements");
			return values[elements / 2];
		}
	};

	struct circularList_t {
	private:
		int *values;
		int length;
		int head = 0, tail = 0;
	public:
		circularList_t(int length) {
			values = new int[length];
			this->length = length;
		}
		void add(int value) {
			values[head++] = value;
			head %= length;
			if (head == tail) {
				tail++;
				tail %= length;
			}
		}
		int operator [] (int index) const {
			return values[(tail + index) % length];
		}
		int& operator [] (int index) {
			return values[(tail + index) % length];
		}
	};

	struct rect {
		int top, bottom;
		int left, right;

		rect() {
			top = bottom = left = right = 0;
		}

		HOST_DEVICE rect(int top, int left, int bottom, int right) {
			this->top = top;
			this->bottom = bottom;
			this->left = left;
			this->right = right;
		}
	};

	struct region {

	};

}