#include "GPUFunctions.h"

#define TILE_DIM 32
#define BLOCK_ROWS 8

namespace parallel_seg {

	__global__ void transpose(const float *idata, float *odata, int width, int height);

	void transposeDiagonal(const float *in, float *out, int width, int height) {
		dim3 block(TILE_DIM, BLOCK_ROWS), grid;
		grid.x = CEIL(width, TILE_DIM); grid.y = CEIL(height, TILE_DIM);
		transpose<<<grid, block>>>(in, out, width, height);
	}

	__global__ void transpose(const float *idata, float *odata, int width, int height) {
		__shared__ float tile[TILE_DIM][TILE_DIM + 1];
		const int size = width * height;
		int x, y; // Pozycja przetwarzanego wycinka
		
		if (width == height) {
			y = blockIdx.x;
			x = (blockIdx.x + blockIdx.y) % gridDim.x;
		} else {
			int bid = blockIdx.x + gridDim.x*blockIdx.y;
			y = bid % gridDim.y;
			x = ((bid / gridDim.y) + y) % gridDim.x;
		}
		
		int xIndex = x * TILE_DIM + threadIdx.x;
		int yIndex = y * TILE_DIM + threadIdx.y;
		int indexIn = xIndex + (yIndex) * width;
		
		xIndex = y * TILE_DIM + threadIdx.x;
		yIndex = x * TILE_DIM + threadIdx.y;
		int indexOut = xIndex + (yIndex) * height;
		
		if ((x * TILE_DIM + threadIdx.x) < width) { // Przyrównane indeksy po transponowaniu
			for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) {
				//if (indexIn < size)
				tile[threadIdx.y + i][threadIdx.x] = idata[indexIn < size ? indexIn : 0];
				indexIn += BLOCK_ROWS * width;
			}
		}

		__syncthreads();

		if (xIndex < height) {
			for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) {
				if (indexOut < size)
					odata[indexOut] = tile[threadIdx.x][threadIdx.y + i];
				indexOut += BLOCK_ROWS * height;
			}
		}
	}

}