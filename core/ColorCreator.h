#pragma once

#include <cstdlib>
#include "Types.h"

namespace parallel_seg {

	class ColorCreator {
	public:
		pixel3c getColor() const;
	};

}