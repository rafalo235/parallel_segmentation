#include "GPUFunctions.h"

#define TILE_DIM 32
#define BLOCK_ROWS 8

namespace parallel_seg {

	__global__ void copyToInt(const float *in, float *out, int width, int height);

	void copyToIntegral(const float *in, float *out, int width, int height) {
		dim3 block(TILE_DIM, BLOCK_ROWS), grid;
		grid.x = CEIL(width, TILE_DIM); grid.y = CEIL(height, TILE_DIM);
		copyToInt<<<grid, block>>>(in, out, width, height);
	}

	__global__ void copyToInt(const float *in, float *out, int width, int height) {
		const int x = blockIdx.x * TILE_DIM + threadIdx.x;
		const int y = blockIdx.y * TILE_DIM + threadIdx.y;
		const int size = width * height;

		int index = width * y + x;
		int indexOut = (width + 1) * (y + 1) + x + 1;

		if (x < width) {
			for (int i = 0; i < TILE_DIM; i += BLOCK_ROWS) {
				if (index < size) {
					out[indexOut] = in[index];
					if (x == 0)
						out[indexOut - 1] = 0.0f;
					index += width * BLOCK_ROWS;
					indexOut += (width + 1) * BLOCK_ROWS;
				}
				else {
					break;
				}
			}
			if (y == 0)
				out[x + 1] = 0.0f;
		}

		if (x == 0 && y == 0)
			out[0] = 0.0f;
	}

}