#include "core.h"

#include "GPUFunctions.h"
#include "Types.h"
#include <cuda.h>

namespace parallel_seg {

	__global__ void sumRows(float *out, int width, int height, int strideOut);
	__global__ void sumColumns(float *out, int width, int height, int strideOut);
	__global__ void sumColumns(const float *in, float *out, int width, int height, int strideOut);

	double imageIntegralRowCol(const float* in, float **outPtr, int width, int height) {
		dim3 grid, block;
		cudaEvent_t start, stop;
		float *out = *outPtr;
		float elapsedTime;
		int widthIntegral = width + 1;

		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		// Sumowanie kolumn
		block.x = horizontalBlockSize; block.y = 1;
		grid.x = CEIL(width, horizontalBlockSize); grid.y = 1;
		sumColumns<<<grid, block>>>(in, out, width, height, widthIntegral);

		// Sumowanie wierszy
		block.x = 1; block.y = verticalBlockSize;
		grid.x = 1; grid.y = CEIL(height, verticalBlockSize);
		sumRows<<<grid, block>>>(&out[widthIntegral], width, height, widthIntegral); // normalne rozmiary

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	double imageIntegralRowColT(const float* in, float **outPtr, int width, int height) {
		float *tmp;
		float *out = *outPtr;
		dim3 grid, block;
		int widthIntegral = width + 1,
			heightIntegral = height + 1;
		cudaEvent_t start, stop;
		float elapsedTime;

		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		if (cudaMalloc((void**)&tmp, widthIntegral * heightIntegral * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();

		// Sumowanie kolumn
		block.x = horizontalBlockSize; block.y = 1;
		grid.x = CEIL(width, horizontalBlockSize); grid.y = 1;
		sumColumns<<<grid, block>>>(in, out, width, height, widthIntegral); // normalne rozmiary

		transposeDiagonal(out, tmp, widthIntegral, heightIntegral);

		// Sumowanie wczesniejszych wierszy, wyczyszczenie tablicy (dla 0 na brzegu) i ponowna transpozycja
		block.x = verticalBlockSize; block.y = 1;
		grid.x = CEIL(height, verticalBlockSize); grid.y = 1;
		sumColumns<<<grid, block>>>(&tmp[1], height, width, heightIntegral);
		
		transposeDiagonal(tmp, out, heightIntegral, widthIntegral);
		cudaFree(tmp);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	__global__ void sumRows(float *out, int width, int height, int strideOut) {
		int indexOut = blockDim.y * blockIdx.y + threadIdx.y;
		
		if (indexOut < height) {
			float sum = 0.0f;
			
			indexOut *= strideOut;

			out[indexOut] = 0.0f;
			++indexOut;
			for (int counter = 0; counter < width; ++counter, ++indexOut) {
				sum += out[indexOut];
				out[indexOut] = sum; // pierwsza komorka zerowa
			}
		}
	}

	__global__ void sumColumns(float *out, int width, int height, int strideOut) {
		int indexOut = blockDim.x * blockIdx.x + threadIdx.x;
		
		if (indexOut < width) {
			float sum = 0.0f;

			//endOut = indexOut + height * strideOut;

			out[indexOut] = 0.0f;
			indexOut += strideOut;
			for (int counter = 0; counter < height; counter++, indexOut += strideOut) {
				sum += out[indexOut];
				out[indexOut] = sum;
			}
		}
	}

	__global__ void sumColumns(const float *in, float *out, int width, int height, int strideOut) {
		int indexIn = blockDim.x * blockIdx.x + threadIdx.x;
		int indexOut = indexIn + 1; // przesuniecie w prawo rozpoczecie od 1 wiersza

		if (indexIn == 0)
			out[0] = 0.0f;

		if (indexIn < width) {
			float sum = 0.0f;

			for (int counter = 0; counter < height;
					++counter, 
					indexIn += width, 
					indexOut += strideOut) {
				out[indexOut] = sum;
				sum += in[indexIn];
			}
			out[indexOut] = sum;
		}
	}


}