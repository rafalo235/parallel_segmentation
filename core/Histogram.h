#pragma once

#include "Types.h"

namespace parallel_seg {

struct hnode {
	pixel3c pixel;
	unsigned int amount;
	hnode *parent = nullptr, *left = nullptr, *right = nullptr;

	hnode(const pixel3c& pixel, const hnode* parent) {
		this->pixel = pixel;
		this->parent = (hnode*)parent;
		amount = 0;
	}
};

class Histogram
{
private:


public:
	hnode *root = nullptr;


	Histogram();
	~Histogram();

	void add(const pixel3c& pixel);
	void balance();
	void balance(const pixel3c& pixel);
	hnode* get(const pixel3c& value) const;

	unsigned int sumValues() const;
	uint3 colorValueMultiSum() const;
	
	uint3 sortedColorValueMultiSum(const hnode* start, const pixel3c& pixel) const;
	unsigned int sortedValueSum(const hnode* start, const pixel3c& pixel) const;

	static void colorValueMultiply(const hnode& node, uint3& sum);

	static hnode* first(const hnode* node);
	static hnode* next(const hnode* node);
	static hnode* last(const hnode* node);
	static hnode* search(const hnode* node, const hnode** parent, const pixel3c& value);
};

}
