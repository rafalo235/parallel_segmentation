#include "Histogram.h"

#include <stack>
#include <list>
#include <algorithm>

namespace parallel_seg {

Histogram::Histogram()
{
}


Histogram::~Histogram() {
	std::stack<hnode*> stack;
	if (this->root != nullptr)
		stack.push(this->root);
	while (!stack.empty()) {
		hnode* pointer = stack.top(); stack.pop();
		if (pointer->right != nullptr)
			stack.push(pointer->right);
		if (pointer->left != nullptr)
			stack.push(pointer->left);
		delete pointer;
	}
}

void Histogram::add(const pixel3c& pixel) {
	hnode *pointer, *parent;
	pointer = search(root, (const hnode**)&parent, pixel);
	if (pointer != nullptr)
		pointer->amount++;
	else if (parent != nullptr) {
		if (pixel > parent->pixel)
			parent->right = new hnode(pixel, parent);
		else
			parent->left = new hnode(pixel, parent);
	}
	else
		this->root = new hnode(pixel, nullptr);
}

hnode* Histogram::get(const pixel3c& value) const {
	hnode* parent;
	hnode* p = search(this->root, (const hnode**)&parent, value);
	return p != nullptr ? p : parent;
}

void Histogram::balance() {

}

void Histogram::balance(const pixel3c& root) {
	hnode *parent = this->root,
		*sibling = nullptr;
	if (parent != nullptr || parent->pixel == root) {
		return;
	}
	//if (root > parent->pixel)
	//	sibling = parent->right;
	//else
	//	sibling = parent->left;
	//
	//while (sibling != nullptr) {
	//	if (root == sibling->pixel) {
	//		if (parent->pixel < sibling->pixel ) { // sprawdzenie po kt�rej stronie jest rodze�stwo

	//		} else {

	//		}
	//	}


	//}
	sibling = search(this->root, (const hnode**)&parent, root);
}

unsigned int Histogram::sumValues() const {
	std::stack<hnode*> nodes;
	unsigned int sum = 0.0;
	if (this->root != nullptr)
		nodes.push(this->root);
	while (!nodes.empty()) {
		hnode* n = nodes.top(); nodes.pop();
		sum += n->amount;
		if (n->right != nullptr)
			nodes.push(n->right);
		if (n->left != nullptr)
			nodes.push(n->left);
	}
	return sum;
}

void Histogram::colorValueMultiply(const hnode& node, uint3& sum) {
	sum.x += node.pixel[0] * node.amount;
	sum.y += node.pixel[1] * node.amount;
	sum.z += node.pixel[2] * node.amount;
}

uint3 Histogram::colorValueMultiSum() const {
	uint3 sum; sum.x = 0; sum.y = 0; sum.z = 0;
	std::stack<hnode*> nodes;

	if (this->root != nullptr) nodes.push(this->root);
	
	while (!nodes.empty()) {
		hnode* n = nodes.top(); nodes.pop();
		colorValueMultiply(*n, sum);

		if (n->right != nullptr) nodes.push(n->right);
		if (n->left != nullptr) nodes.push(n->left);
	}

	return sum;
}

hnode* Histogram::first(const hnode* node) {
	while (node->left != nullptr) node = node->left;
	return (hnode*)node;
}

hnode* Histogram::next(const hnode* node) {
	if (node->right != nullptr)
		return first(node->right);
	else {
		hnode* n = (hnode*)node;
		while (n->parent != nullptr && n == n->parent->right) {
			n = n->parent;
		}
		return n->parent;
	}
}

hnode* Histogram::last(const hnode* node) {
	while (node->right != nullptr) node = node->right;
	return (hnode*)node;
}

uint3 Histogram::sortedColorValueMultiSum(const hnode* start, const pixel3c& pixel) const {
	//hnode* n = first(this->root);
	uint3 sum; sum.x = 0; sum.y = 0; sum.z = 0;
	while (start != nullptr) {
		Histogram::colorValueMultiply(*start, sum);
		if (start->pixel == pixel)
			break;
		start = Histogram::next(start);
	}
	return sum;
}

unsigned int Histogram::sortedValueSum(const hnode* start, const pixel3c& pixel) const {
	//hnode* n = first(this->root);
	unsigned int sum = 0;
	while (start != nullptr) {
		sum += start->amount;
		if (start->pixel == pixel)
			break;
		start = Histogram::next(start);
	}
	return sum;
}



hnode* Histogram::search(const hnode* node, const hnode** parent, const pixel3c& value) {
	*parent = nullptr;
	while (node != nullptr) {
		if (value == node->pixel)
			return (hnode*)node;
		*parent = node;
		if (value > node->pixel)
			node = node->right;
		else
			node = node->left;
	}
	return nullptr;
}

}
