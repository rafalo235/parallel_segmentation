#include "LuminanceHistogram.h"

#include "Types.h"

#include <fstream>
#include <iostream>
#include "CPUFunctions.h"

#define WINDOW_RADIUS 2
#define SMOOTH_WINDOW_RADIUS 4
#define SMOOTH_WINDOW_LENGTH 5

typedef enum {
	LEFT, MAX, RIGHT
} searchState_t;

namespace parallel_seg {

	LuminanceHistogram::LuminanceHistogram() {
		tab = new int[MAX_COLOR_VAL + 10];
		for (int i = 0; i < MAX_COLOR_VAL + 10; i++) tab[i] = 0u;
		length = MAX_COLOR_VAL + 10;
	}

	LuminanceHistogram::~LuminanceHistogram() {
		delete[] tab;
	}

	void LuminanceHistogram::add(const pixel3c &pixel) {
		int lum = (int)(pixel.luminance());
		if (lum > 254)
			lum = 255;
		tab[lum]++;
	}

	void LuminanceHistogram::save(const wchar_t* path) const {
		std::ofstream file;
		file.open(path, std::ios::out);
		for (unsigned int i = 0; i < length; i++) {
			file << i << '\t' << tab[i] << std::endl;
		}
		file.close();
	}

	void LuminanceHistogram::getLocalExtrema(std::list<channel> &minimas, std::list<channel> &maximas) const {
		for (int i = WINDOW_RADIUS; i < length; i++) {
			channel last = i - WINDOW_RADIUS;
			channel first = i;
			channel max = i, min = i;

			for (int j = 1; j <= WINDOW_RADIUS; j++) {
				int index = i - j;
				if (tab[index] > tab[max]) {
					max = index;
				} else if (tab[index] < tab[min]) {
					min = index;
				}
			}

			//bool flag = false;
			if (max != last && max != first) {
				maximas.push_back(max);
				//flag = true;
			}
			if (min != last && min != first) {
				minimas.push_back(min);
				//flag = true;
			}
			/*if (flag)
				i += WINDOW_RADIUS - 2;*/
		}
		std::cout << "Minimas" << std::endl;
		for (std::list<channel>::iterator i = minimas.begin(); i != minimas.end(); i++) {
			std::cout << "tab[" << (int)*i << "] = " << tab[*i] << std::endl;
		}
		std::cout << "Maximas" << std::endl;
		for (std::list<channel>::iterator i = maximas.begin(); i != maximas.end(); i++) {
			std::cout << "tab[" << (int)*i << "] = " << tab[*i] << std::endl;
		}
	}

	void LuminanceHistogram::smooth() {
		for (int i = 0; i < length - SMOOTH_WINDOW_RADIUS; i++) {
			unsigned mean = 0;
			for (int j = i; j <= i + SMOOTH_WINDOW_RADIUS; j++) {
				mean += tab[j];
			}
			tab[i] = mean / SMOOTH_WINDOW_LENGTH;
		}
	}

#define THRESHOLD		2
#define MINIMUM_DERIVATIVE_VALUE_POS			(1)
#define MINIMUM_DERIVATIVE_VALUE_NEG			(-1)

	void threshold(int *data, unsigned int length, int threshold) {
		int thresholdNegative = -threshold;
		for (int i = 0; i < length; i++) {
			if (data[i] < threshold && data[i] > thresholdNegative)
				data[i] = 0;
		}
	}

	std::list<segment>* LuminanceHistogram::getSegments() const {
		std::list<segment>* segments = new std::list<segment>();
		int **dataSeries = new int*[2];
		int counter, counter2;
		
		parallel_seg::filterMean(this->tab, length, 3);
		//parallel_seg::filterMedian(this->tab, length, 5);

		int* derivative = derivate(this->tab, this->length);
		parallel_seg::filterMean(derivative, length, 1);

		int *doubleDerivative = derivate(derivative, length);
		//parallel_seg::filterMedian(doubleDerivative, length, 5);

		int *zeroTab = (int*)calloc(length, sizeof(int));
		int *zeros = getZeroPoints(derivative, length, counter);
		int *zerosDouble = getZeroPoints(doubleDerivative, length, counter2);

		//threshold(doubleDerivative, length, THRESHOLD);

		int* indices = getIndices(length);
		dataSeries[0] = indices;

		dataSeries[1] = this->tab;
		saveCSVData(L"D:\\Udostepniony\\seria.csv", (const int**)dataSeries, 2, this->length);
		dataSeries[1] = derivative;
		saveCSVData(L"D:\\Udostepniony\\pochodna.csv", (const int**)dataSeries, 2, this->length);
		dataSeries[1] = doubleDerivative;
		saveCSVData(L"D:\\Udostepniony\\pochodna2.csv", (const int**)dataSeries, 2, this->length);
		dataSeries[1] = zeroTab;
		dataSeries[0] = zeros;
		saveCSVData(L"D:\\Udostepniony\\pochodna_mz.csv", (const int**)dataSeries, 2, counter);
		dataSeries[0] = zerosDouble;
		saveCSVData(L"D:\\Udostepniony\\pochodna2_mz.csv", (const int**)dataSeries, 2, counter2);

		/*for (int i = 1; i < length - 2; i++) {
			if (doubleDerivative[i - 1] > 0 && doubleDerivative[i] < 0) {

			}
		}*/

		segment* s = new segment;
		s->left = 0;

		for (int i = 0; i < counter2; i++) {
			// Sprawdz znak pierwszej pochodnej w tym punkcie zerowym
			if (derivative[zerosDouble[i]] > MINIMUM_DERIVATIVE_VALUE_POS) {
				s->left = zerosDouble[i];
				while (s->left > 0 && derivative[s->left] > 0) {
					s->left--;
				}
			}
			else if (derivative[zerosDouble[i]] < MINIMUM_DERIVATIVE_VALUE_NEG) {
				s->right = zerosDouble[i];
				if (s->left != -1) {
					while (s->right < length && derivative[s->right] < 0) {
						s->right++;
					}
					s->max = (s->right + s->left) / 2;
					segments->push_back(*s);
					s = new segment;
					s->left = -1;
				}
			}
		}

		delete[] derivative;
		delete[] doubleDerivative;
		delete[] indices;
		delete[] zerosDouble;
		delete[] zeros;
		delete[] zeroTab;
		delete[] dataSeries;

		return segments;
	}

	/*std::list<segment> *LuminanceHistogram::getSegments() const {
		std::list<channel> minimas, maximas;
		std::list<segment> *segments = new std::list<segment>();
		std::list<channel>::iterator mn, mx;

		this->getLocalExtrema(minimas, maximas);
		mn = minimas.begin(); mx = maximas.begin();

		while (mx != maximas.end()) {
			segment s;
			
			if (mn == minimas.end()) {
				s.left = segments->back().right;
			} else if (*mn < *mx) {
				s.left = *mn;
				mn++;
			}
			else {
				s.left = 0;
			}

			if (mn != minimas.end() && *mn > *mx) {
				s.right = *mn;
			}
			else {
				s.right = MAX_COLOR_VAL;
			}

			s.max = *mx;
			mx++;

			if (s.left < s.max && s.right > s.max)
				segments->push_back(s);
		}
		

		return segments;
	}*/

	void LuminanceHistogram::printSegments(std::list<segment> &segments) {
		for (std::list<segment>::iterator i = segments.begin(); i != segments.end(); i++) {
			std::cout << "<" << (int)(*i).left << ", " << (int)(*i).max << ", " << (int)(*i).right << ">" << std::endl;
		}
	}

}