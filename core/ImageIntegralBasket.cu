#include "core.h"

#include "GPUFunctions.h"
#include "Types.h"
#include <cuda.h>

#define ELEMENTS_PER_THREAD		2

namespace parallel_seg {

	// Sumowanie w jednym bloku
	__global__ void sumRowsBasket(const float *in, float *out, int width, int height, int stride);
	__global__ void sumRowsBasket(float *out, int width, int height, int stride);
	__global__ void sumColsBasket(float *out, int width, int height, int stride);

	// Rozdzielenie na wiele blok�w
	__global__ void sumRowsBasket(const float *__restrict__ in, float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride);
	__global__ void sumColsBasket(float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride);

	double imageIntegralRowColBasketOneBlock(const float *in, float **outPtr, int width, int height) {
		float elapsedTime, *out = *outPtr;
		int widthIntegral = width + 1;
		cudaEvent_t start, stop;
		dim3 block, grid;

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsBasket<<<grid, block, width * sizeof(float)>>>(in, &out[widthIntegral], width, height, widthIntegral);

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = 1;
		sumColsBasket<<<grid, block, height * sizeof(float)>>>(out, width, height, widthIntegral);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	double imageIntegralRowColBasket(const float *in, float **outPtr, int width, int height) {
		dim3 grid, block;
		cudaEvent_t start, stop;
		int widthIntegral = width + 1;
		float elapsedTime, *lastElements, *out = *outPtr;
		int lastElementWidth = CEIL(width, ELEMENTS_PER_THREAD * horizontalBlockSize);
		int lastElementHeight = CEIL(height, ELEMENTS_PER_THREAD * verticalBlockSize);

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);
		
		// Sumowanie wierszy

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = lastElementWidth; grid.y = height;
		if (cudaMalloc((void**)&lastElements, (lastElementWidth + 1) * height * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		sumRowsBasket<<<grid, block, ELEMENTS_PER_THREAD * block.x * sizeof(float)>>>(in, &out[widthIntegral], lastElements, width, height, widthIntegral);

		block.x = lastElementWidth < 512 ? lastElementWidth : 512; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsBasket<<<grid, block, lastElementWidth * sizeof(float)>>>(lastElements, lastElementWidth, height, lastElementWidth + 1);

		addHorizontalLastElements(lastElements, &out[widthIntegral + 1], width, height, ELEMENTS_PER_THREAD * horizontalBlockSize);
		cudaFree(lastElements);

		// Sumowanie kolumn

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = lastElementHeight;
		if (cudaMalloc((void**)&lastElements, (lastElementHeight + 1) * width * sizeof(float)) != cudaSuccess)
			throw std::bad_alloc();
		sumColsBasket<<<grid, block, ELEMENTS_PER_THREAD * block.y * sizeof(float)>>>(out, lastElements, width, height, widthIntegral);

		block.x = lastElementHeight < 512 ? lastElementHeight : 512; block.y = 1;
		grid.x = 1; grid.y = width;
		sumRowsBasket<<<grid, block, lastElementHeight * sizeof(float)>>>(lastElements, lastElementHeight, width, lastElementHeight + 1);
		
		addVerticalLastElements(lastElements, &out[widthIntegral + 1], width, height, ELEMENTS_PER_THREAD * verticalBlockSize);
		cudaFree(lastElements);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	// ~~~~~~~~~~~Sumowanie w jednym bloku ~~~~~~~~~~~~~~~~~~~~

	__global__ void sumRowsBasket(const float *in, float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		const int widthDiv2 = width / 2;

		// Przypisanie zera do pierwszej kolumny, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.x == 0)
			out[blockIdx.y * stride] = 0.0f;
		// pominieta pierwsza kolumna
		for (int i = threadIdx.x; i < width; i += blockDim.x)
			temp[i] = in[blockIdx.y * width + i];
		__syncthreads();

		for (int s = 2, sDiv2 = 1; sDiv2 < width; s <<= 1, sDiv2 <<= 1) {
			for (int i = threadIdx.x; i < widthDiv2; i += blockDim.x) {
				// (i / sDiv2) = numer koszyka
				// s - odleg�o�� miedzy koszykami
				int ai = (i / sDiv2) * s + sDiv2 - 1;
				int bi = ai + (i % sDiv2) + 1;

				if (bi < width)
					temp[bi] += temp[ai];
			}
			__syncthreads();
		}

		for (int i = threadIdx.x; i < width; i += blockDim.x)
			out[blockIdx.y * stride + i + 1] = temp[i];
	}

	__global__ void sumRowsBasket(float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		const int widthDiv2 = width / 2;

		// Przypisanie zera do pierwszej kolumny, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.x == 0)
			out[blockIdx.y * stride] = 0.0f;
		// pominieta pierwsza kolumna
		for (int i = threadIdx.x; i < width; i += blockDim.x)
			temp[i] = out[blockIdx.y * stride + i + 1];
		__syncthreads();

		for (int s = 2, sDiv2 = 1; sDiv2 < width; s <<= 1, sDiv2 <<= 1) {
			for (int i = threadIdx.x; i < widthDiv2; i += blockDim.x) {
				// (i / sDiv2) = numer koszyka
				// s - odleg�o�� miedzy koszykami
				int ai = (i / sDiv2) * s + sDiv2 - 1;
				int bi = ai + (i % sDiv2) + 1;

				if (bi < width)
					temp[bi] += temp[ai];
			}
			__syncthreads();
		}

		for (int i = threadIdx.x; i < width; i += blockDim.x)
			out[blockIdx.y * stride + i + 1] = temp[i];
	}

	__global__ void sumColsBasket(float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		const int heightDiv2 = height / 2;
#if __CUDA_ARCH__ < 200
		const int offset = resolveOffset(blockIdx.x, width);
#else
		const int offset = blockIdx.x;
#endif

		// Przypisanie zera do pierwszego wiersza, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.y == 0) {
			out[offset + 1] = 0.0f;
			if (offset == 0)
				out[0] = 0.0f;
		}
		// pominieta pierwsza kolumna
		for (int i = threadIdx.y; i < height; i += blockDim.y)
			temp[i] = out[(i + 1) * stride + offset + 1];
		__syncthreads();

		for (int s = 2, sDiv2 = 1; sDiv2 < height; s <<= 1, sDiv2 <<= 1) {
			for (int i = threadIdx.y; i < heightDiv2; i += blockDim.y) {
				// (i / sDiv2) = numer koszyka
				// s - odleg�o�� miedzy koszykami
				int ai = (i / sDiv2) * s + sDiv2 - 1;
				int bi = ai + (i % sDiv2) + 1;

				if (bi < height)
					temp[bi] += temp[ai];
			}
			__syncthreads();
		}

		for (int i = threadIdx.y; i < height; i += blockDim.y)
			out[(i + 1) * stride + offset + 1] = temp[i];
	}

	// ~~~~~~~~~~~Sumowanie w wielu blokach ~~~~~~~~~~~~~~~~~~~~

	__global__ void sumRowsBasket(const float *__restrict__ in, float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride) {
		extern __shared__ float temp[];
		const int elements = ELEMENTS_PER_THREAD * blockDim.x;
		int blockOffset = blockIdx.x * elements;

		if (threadIdx.x == 0 && blockIdx.x == 0) // przekaza� wska�nik na drugi wiersz
			out[blockIdx.y * stride] = 0.0f;

		for (int i = threadIdx.x; i < elements; i += blockDim.x) {
			if (blockOffset + i < width)
				temp[i] = in[blockIdx.y * width + blockOffset + i];
			else
				temp[i] = 0.0f;
		}
		__syncthreads();

		for (int s = 2, sDiv2 = 1; sDiv2 < elements; s <<= 1, sDiv2 <<= 1) {
			// (i / sDiv2) = numer koszyka
			// s - odleg�o�� miedzy koszykami
			int ai = (threadIdx.x / sDiv2) * s + sDiv2 - 1;
			int bi = ai + (threadIdx.x % sDiv2) + 1;
			if (bi < elements)
				temp[bi] += temp[ai];
			__syncthreads();
		}

		for (int i = threadIdx.x; i < elements; i += blockDim.x) {
			if (blockOffset + i < width)
				out[blockIdx.y * stride + blockOffset + i + 1] = temp[i];
		}
		if (threadIdx.x == blockDim.x - 1) {
			lastElements[(gridDim.x + 1) * blockIdx.y + blockIdx.x + 1] = temp[elements - 1]; //zerowane [x][0] przy obliczaniu sumy
		}
	}

	__global__ void sumColsBasket(float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride) {
		extern __shared__ float temp[];
		const int elements = ELEMENTS_PER_THREAD * blockDim.y;
#if __CUDA_ARCH__ < 200
		const int xOffset = resolveOffset(blockIdx.x, width);
#else
		const int xOffset = blockIdx.x;
#endif
		int blockOffset = blockIdx.y * elements;

		// Zerowanie pierwszego wiersza
		if (threadIdx.y == 0 && blockIdx.y == 0) {
			out[xOffset + 1] = 0.0f;
			if (blockIdx.x == 0)
				out[0] = 0.0f;
		}

		for (int i = threadIdx.y; i < elements; i += blockDim.y) {
			if (blockOffset + i < height)
				temp[i] = out[(blockOffset + i + 1) * stride + xOffset + 1];
			else
				temp[i] = 0.0f;
		}
		__syncthreads();

		for (int s = 2, sDiv2 = 1; sDiv2 < elements; s <<= 1, sDiv2 <<= 1) {
			// (i / sDiv2) = numer koszyka
			// s - odleg�o�� miedzy koszykami
			int ai = (threadIdx.y / sDiv2) * s + sDiv2 - 1;
			int bi = ai + (threadIdx.y % sDiv2) + 1;
			if (bi < elements)
				temp[bi] += temp[ai];
			__syncthreads();
		}

		for (int i = threadIdx.y; i < elements; i += blockDim.y) {
			if (blockOffset + i < height)
				out[(blockOffset + i + 1) * stride + xOffset + 1] = temp[i];
		}
		if (threadIdx.y == blockDim.y - 1) // transpozycja macierzy z sumami
			lastElements[(gridDim.y + 1) * xOffset + blockIdx.y + 1] = temp[elements - 1];
	}

}