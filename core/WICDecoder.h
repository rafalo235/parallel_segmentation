#pragma once

#include "IDecoder.h"

#ifdef PARALLEL_SEGMENTATION_DLL_EXPORTS
#define PARALLEL_SEGMENTATION_API __declspec(dllexport) 
#else
#define PARALLEL_SEGMENTATION_API __declspec(dllimport) 
#endif

struct IWICImagingFactory;

namespace parallel_seg {

	class WICDecoder : public IDecoder {
	private:
		IWICImagingFactory *_factory;
		
		Image3c* parse(unsigned char *data, unsigned int size, unsigned int width, unsigned int height, bool toDevice);
		void RGBtoBGR(unsigned char *data, unsigned int size);
	public:
		PARALLEL_SEGMENTATION_API void initialize();
		PARALLEL_SEGMENTATION_API void uninitialize();
		PARALLEL_SEGMENTATION_API Image3c* decode(wchar_t *path, bool toDevice);
		PARALLEL_SEGMENTATION_API void encode(const Image3c &image, wchar_t *path);
	};

}