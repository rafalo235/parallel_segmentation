#include "core.h"

#include "GPUFunctions.h"
#include "Types.h"
#include <cuda.h>

#define BANKS			32
#define BANKS_LOG		5
#define AVOID_BANK_CONFLICTS
#ifdef AVOID_BANK_CONFLICTS
#define INDEX(x)		((x) + ((x) >> BANKS_LOG))
#define EXTEND_MEM(x)	((x) + ((x) >> BANKS_LOG))
#else
#define INDEX(x)		(x)
#define EXTEND_MEM(x)	(x)
#endif

namespace parallel_seg {

	// Sumowanie w jednym bloku w�tk�w
	__global__ void sumRowsEfficient(const float *in, float *out, int width, int height, int stride, int sharedLength);
	__global__ void sumRowsEfficient(float *out, int width, int height, int stride, int sharedLength);
	__global__ void sumColsEfficient(float *out, int width, int height, int stride, int sharedLength);

	// Sumowanie w wielu blokach
	__global__ void sumRowsEfficient(const float *__restrict__ in, float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride, int sharedLength);
	__global__ void sumColsEfficient(float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride, int sharedLength);

	double imageIntegralRowColEfficientOneBlock(const float *in, float **outPtr, int width, int height) {
		float elapsedTime, *out = *outPtr;
		const int widthIntegral = width + 1;
		const int sharedMemoryWidth = 1 << ((int)ceilf(log2f(width)));
		const int sharedMemoryHeight = 1 << ((int)ceilf(log2f(height)));
		cudaEvent_t start, stop;
		dim3 block, grid;

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsEfficient<<<grid, block, sharedMemoryWidth * sizeof(float)>>>(in, out, width, height, widthIntegral, sharedMemoryWidth);

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = 1;
		sumColsEfficient<<<grid, block, sharedMemoryHeight * sizeof(float)>>>(out, width, height, widthIntegral, sharedMemoryHeight);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

#define ELEMENTS_PER_THREAD_HORIZONTAL	8
#define ELEMENTS_PER_THREAD_VERTICAL	2

#define CEIL_2_POW(x)		(1 << ((int)ceilf(log2f(x))))

	double imageIntegralRowColEfficient(const float *in, float **outPtr, int width, int height) {
		dim3 grid, block;
		cudaEvent_t start, stop;
		const int widthIntegral = width + 1;
		float elapsedTime, *lastElements, *out = *outPtr;
		const int sharedMemoryWidth = CEIL_2_POW(ELEMENTS_PER_THREAD_HORIZONTAL * horizontalBlockSize);
		const int sharedMemoryHeight = CEIL_2_POW(ELEMENTS_PER_THREAD_VERTICAL * verticalBlockSize);
		const int lastElementWidth = CEIL(width, sharedMemoryWidth);
		const int lastElementHeight = CEIL(height, sharedMemoryHeight);

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		// Sumowanie wierszy

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = lastElementWidth; grid.y = height;
		if (cudaMalloc((void**)&lastElements, (lastElementWidth + 1) * height * sizeof(float)) != cudaSuccess) return 0.0;
		sumRowsEfficient<<<grid, block, EXTEND_MEM(sharedMemoryWidth) * sizeof(float)>>>(in, &out[widthIntegral], lastElements, width, height, widthIntegral, sharedMemoryWidth);

		int lastElementSharedWidth = CEIL_2_POW(lastElementWidth);
		block.x = CEIL(lastElementSharedWidth, 2) < 512 ? CEIL(lastElementSharedWidth, 2) : 512; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsEfficient<<<grid, block, lastElementSharedWidth * sizeof(float)>>>(lastElements, lastElementWidth, height, lastElementWidth + 1, lastElementSharedWidth);

		addHorizontalLastElements(lastElements, &out[widthIntegral + 1], width, height, sharedMemoryWidth);
		cudaFree(lastElements);

		//// Sumowanie kolumn

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = lastElementHeight;
		if (cudaMalloc((void**)&lastElements, (lastElementHeight + 1) * width * sizeof(float)) != cudaSuccess) return 0.0;
		sumColsEfficient<<<grid, block, EXTEND_MEM(sharedMemoryHeight) * sizeof(float)>>>(out, lastElements, width, height, widthIntegral, sharedMemoryHeight);

		int lastElementSharedHeight = CEIL_2_POW(lastElementHeight);
		block.x = CEIL(lastElementSharedHeight, 2) < 512 ? CEIL(lastElementSharedHeight, 2) : 512; block.y = 1;
		grid.x = 1; grid.y = width;
		sumRowsEfficient<<<grid, block, lastElementSharedHeight * sizeof(float)>>>(lastElements, lastElementHeight, width, lastElementHeight + 1, lastElementSharedHeight);

		addVerticalLastElements(lastElements, &out[widthIntegral + 1], width, height, sharedMemoryHeight);
		cudaFree(lastElements);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	// ~~~~~~~~~~~~~~~Przetwarzanie danych w jednym bloku na wiersz/kolumne~~~~~~~~~~~
	
	__global__ void sumRowsEfficient(const float *in, float *out, int width, int height, int stride, int sharedLength) {
		extern __shared__ float temp[];
		int offset = 1;

		for (int i = threadIdx.x; i < sharedLength; i += blockDim.x) {
			if (i < width)
				temp[i] = in[blockIdx.y * width + i];
			else
				temp[i] = 0.0f;
		}
		__syncthreads();
		
		for (int d = sharedLength >> 1; d > 0; d >>= 1) {

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				temp[bi] += temp[ai];
			}
			offset <<= 1;
			__syncthreads();
		}

		if (threadIdx.x == 0){
			if (width == sharedLength)
				out[blockIdx.y * stride + width] = temp[sharedLength - 1];
			temp[sharedLength - 1] = 0.0f;
		}

		for (int d = 1; d < sharedLength; d <<= 1) {
			offset >>= 1;
			__syncthreads();

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				float swp = temp[ai];
				temp[ai] = temp[bi];
				temp[bi] += swp;
			}
		}

		__syncthreads();
		// Obraz ca�kowy o jedn� kom�rk� d�u�szy
		for (int i = threadIdx.x; i <= width && i < sharedLength; i += blockDim.x)
			out[blockIdx.y * stride + i] = temp[i];
	}

	__global__ void sumRowsEfficient(float *out, int width, int height, int stride, int sharedLength) {
		extern __shared__ float temp[];
		int offset = 1;

		for (int i = threadIdx.x; i < sharedLength; i += blockDim.x) {
			if (i < width)
				temp[i] = out[blockIdx.y * stride + i];
			else
				temp[i] = 0.0f;
		}
		__syncthreads();

		for (int d = sharedLength >> 1; d > 0; d >>= 1) {

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				temp[bi] += temp[ai];
			}
			offset <<= 1;
			__syncthreads();
		}

		if (threadIdx.x == 0){
			if (width == sharedLength)
				out[blockIdx.y * stride + width] = temp[sharedLength - 1];
			temp[sharedLength - 1] = 0.0f;
		}

		for (int d = 1; d < sharedLength; d <<= 1) {
			offset >>= 1;
			__syncthreads();

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				float swp = temp[ai];
				temp[ai] = temp[bi];
				temp[bi] += swp;
			}
		}

		__syncthreads();
		// Obraz ca�kowy o jedn� kom�rk� d�u�szy
		for (int i = threadIdx.x; i <= width && i < sharedLength; i += blockDim.x)
			out[blockIdx.y * stride + i] = temp[i];
	}

	__global__ void sumColsEfficient(float *out, int width, int height, int stride, int sharedLength) {
		extern __shared__ float temp[];
#if __CUDA_ARCH__ < 200
		const int xOffset = resolveOffset(blockIdx.x, width);
#else
		const int xOffset = blockIdx.x;
#endif
		int offset = 1;

		if (threadIdx.y == 0 && xOffset == 0) {
			out[0] = 0.0f;
			out[height * stride] = 0.0f;
		}
		for (int i = threadIdx.y; i < sharedLength; i += blockDim.y) {
			if (i < height)
				temp[i] = out[i * stride + xOffset + 1];
			else
				temp[i] = 0.0f;
		}
		__syncthreads();

		for (int d = sharedLength >> 1; d > 0; d >>= 1) {

			for (int i = threadIdx.y; i < d; i += blockDim.y) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				temp[bi] += temp[ai];
			}
			offset <<= 1;
			__syncthreads();
		}

		if (threadIdx.y == 0){
			if (height == sharedLength)
				out[height * stride + xOffset + 1] = temp[sharedLength - 1];
			temp[sharedLength - 1] = 0.0f;
		}

		for (int d = 1; d < sharedLength; d <<= 1) {
			offset >>= 1;
			__syncthreads();

			for (int i = threadIdx.y; i < d; i += blockDim.y) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				float swp = temp[ai];
				temp[ai] = temp[bi];
				temp[bi] += swp;
			}
		}

		__syncthreads();
		// Obraz ca�kowy o jedn� kom�rk� d�u�szy
		for (int i = threadIdx.y; i <= height && i < sharedLength; i += blockDim.y)
			out[i * stride + xOffset + 1] = temp[i];
	}

	// ~~~~~~~~~~~~~~~~~Przetwarzanie danych w wielu blokach na wiersz/kolumne~~~~~~~~~~~~

	__global__ void sumRowsEfficient(const float *__restrict__ in, float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride, int sharedLength) {
		extern __shared__ float temp[];
		const int blockOffset = blockIdx.x * sharedLength;
		int offset = 1;

		if (threadIdx.x == 0 && blockIdx.x == 0)
			out[blockIdx.y * stride] = 0.0f;

		for (int i = threadIdx.x; i < sharedLength; i += blockDim.x) {
			if (blockOffset + i < width)
				temp[INDEX(i)] = in[blockIdx.y * width + blockOffset + i];
			else
				temp[INDEX(i)] = 0.0f;
		}
		__syncthreads();

		for (int d = sharedLength >> 1; d > 0; d >>= 1) {

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				temp[INDEX(bi)] += temp[INDEX(ai)];
			}
			offset <<= 1;
			__syncthreads();
		}

		if (threadIdx.x == 0) {
			if (blockOffset + sharedLength <= width)
				out[blockIdx.y * stride + blockOffset + sharedLength] = temp[INDEX(sharedLength - 1)];
			lastElements[(gridDim.x + 1) * blockIdx.y + blockIdx.x] = temp[INDEX(sharedLength - 1)];
			temp[INDEX(sharedLength - 1)] = 0.0f;
		}

		for (int d = 1; d < sharedLength; d <<= 1) {
			offset >>= 1;
			__syncthreads();

			for (int i = threadIdx.x; i < d; i += blockDim.x) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				float swp = temp[INDEX(ai)];
				temp[INDEX(ai)] = temp[INDEX(bi)];
				temp[INDEX(bi)] += swp;
			}
		}

		__syncthreads();
		// Pomijana pierwsza komorka w pamiec wspoldzielonej
		for (int i = threadIdx.x + 1; i < sharedLength && blockOffset + i <= width; i += blockDim.x)
			out[blockIdx.y * stride + blockOffset + i] = temp[INDEX(i)];
	}

	__global__ void sumColsEfficient(float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride, int sharedLength) {
		extern __shared__ float temp[];
		const int blockOffset = blockIdx.y * sharedLength;
#if __CUDA_ARCH__ < 200
		const int xOffset = resolveOffset(blockIdx.x, width);
#else
		const int xOffset = blockIdx.x;
#endif
		int offset = 1;

		if (threadIdx.y == 0 && blockIdx.y == 0) {
			out[xOffset + 1] = 0.0f;
			if (xOffset == 0)
				out[0] = 0.0f;
		}

		for (int i = threadIdx.y; i < sharedLength; i += blockDim.y) {
			if (blockOffset + i < height)
				temp[INDEX(i)] = out[(blockOffset + i + 1) * stride + xOffset + 1];
			else
				temp[INDEX(i)] = 0.0f;
		}
		__syncthreads();

		for (int d = sharedLength >> 1; d > 0; d >>= 1) {

			for (int i = threadIdx.y; i < d; i += blockDim.y) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				temp[INDEX(bi)] += temp[INDEX(ai)];
			}
			offset <<= 1;
			__syncthreads();
		}

		if (threadIdx.y == 0){
			if (blockOffset + sharedLength <= height)
				out[(blockOffset + sharedLength) * stride + xOffset + 1] = temp[INDEX(sharedLength - 1)];
			lastElements[(gridDim.y + 1) * xOffset + blockIdx.y] = temp[INDEX(sharedLength - 1)]; // elementy przesuniete przy sumowaniu
			temp[INDEX(sharedLength - 1)] = 0.0f;
		}

		for (int d = 1; d < sharedLength; d <<= 1) {
			offset >>= 1;
			__syncthreads();

			for (int i = threadIdx.y; i < d; i += blockDim.y) {
				int ai = offset * (2 * i + 1) - 1;
				int bi = offset * (2 * i + 2) - 1;

				float swp = temp[INDEX(ai)];
				temp[INDEX(ai)] = temp[INDEX(bi)];
				temp[INDEX(bi)] += swp;
			}
		}

		__syncthreads();
		// Pomijana pierwsza komorka w pamiec wspoldzielonej
		for (int i = threadIdx.y + 1; i < sharedLength && blockOffset + i <= height; i += blockDim.y)
			out[(blockOffset + i) * stride + xOffset + 1] = temp[INDEX(i)];
	}

}