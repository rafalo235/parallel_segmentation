#include "WICDecoder.h"

#include <wincodec.h>
#include <cstring>
#include <cuda_runtime_api.h>

namespace parallel_seg {

	void WICDecoder::initialize() {
#ifndef WIN64
		HRESULT hr = CoInitializeEx(
			nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
		if (FAILED(hr))
			throw "Initialization failed";

		if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&_factory))))
			throw "Cannot create instance decoder";
#endif
	}

	void WICDecoder::uninitialize() {
		_factory->Release();
		CoUninitialize();
	}

	Image3c* WICDecoder::decode(wchar_t *path, bool toDevice) {
#ifndef WIN64
		UINT width = 0, height = 0, size = 0;
		WICRect rectangle = { 0, 0, 0, 0 };
		IWICBitmapDecoder *decoder;
		IWICBitmapFrameDecode *frameOriginal = NULL;
		IWICBitmapSource *frame = NULL;
		IWICBitmap *bitmap = NULL;
		IWICBitmapLock *lock = NULL;
		WICPixelFormatGUID format;
		BYTE *data = NULL;
		Image3c *image = NULL;

		if (FAILED(_factory->CreateDecoderFromFilename(
				path, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &decoder)))
			throw "Cannot create decoder";

		if (FAILED(decoder->GetFrame(0, &frameOriginal)))
			throw "Cannot get frame";

		if (FAILED(frameOriginal->GetPixelFormat(&format)))
			throw "Cannot get pixel format";

		if (!IsEqualGUID(format, GUID_WICPixelFormat24bppRGB))
			if (FAILED(WICConvertBitmapSource(GUID_WICPixelFormat24bppRGB, frameOriginal, &frame)))
				throw "Cannot convert bitmap";
		frameOriginal->Release();

		if (FAILED(frame->GetSize(&width, &height)))
			throw "Cannot get size";
		rectangle.Width = width;
		rectangle.Height = height;

		if (FAILED(_factory->CreateBitmapFromSource(
				frame,
				WICBitmapCacheOnDemand,
				&bitmap)))
			throw "Cannot create bitmap";

		if (FAILED(bitmap->Lock(&rectangle, WICBitmapLockRead, &lock)))
			throw "Cannot lock bitmap";

		if (FAILED(lock->GetDataPointer(&size, &data)))
			throw "Cannot get data pointer";

		image = parse(data, size, width, height, toDevice);

		lock->Release();
		bitmap->Release();
		frame->Release();
		decoder->Release();

		return image;
#else
		return nullptr;
#endif
	}

	void WICDecoder::encode(const Image3c &image, wchar_t *path) {
#ifndef WIN64
		IWICStream *stream = NULL;
		IWICBitmapEncoder *encoder = NULL;
		IWICBitmapFrameEncode *frame = NULL;
		IPropertyBag2 *properties = NULL;

		if (FAILED(_factory->CreateStream(&stream)))
			throw "Cannot create stream";

		if (FAILED(stream->InitializeFromFilename(path, GENERIC_WRITE)))
			throw "Cannot initialize stream";

		if (FAILED(_factory->CreateEncoder(GUID_ContainerFormatTiff, NULL, &encoder)))
			throw "Cannot create encoder";

		if (FAILED(encoder->Initialize(stream, WICBitmapEncoderNoCache)))
			throw "Cannot initialize encoder";

		if (FAILED(encoder->CreateNewFrame(&frame, &properties)))
			throw "Cannot create frame";

		PROPBAG2 option = { 0 };
		option.pstrName = L"TiffCompressionMethod";
		VARIANT varValue;
		VariantInit(&varValue);
		varValue.vt = VT_UI1;
		varValue.bVal = WICTiffCompressionZIP;
		properties->Write(1, &option, &varValue);
		
		if (FAILED(frame->Initialize(properties)))
			throw "Cannot initialize properties";

		frame->SetSize(image.width, image.height);
		WICPixelFormatGUID formatGUID = GUID_WICPixelFormat24bppRGB;
		if (FAILED(frame->SetPixelFormat(&formatGUID)))
			throw "Cannot set pixel format";

		BYTE* data = new BYTE[image.size];

		if (image.onDevice) {
			if (cudaMemcpy(data, image.pixels, image.size, cudaMemcpyDeviceToHost) != cudaSuccess)
				throw "Cannot copy results to host";
		}
		else
			if (!(memcpy(data, image.pixels, image.size)))
				throw "Cannot copy results on host";

		if (!IsEqualGUID(formatGUID, GUID_WICPixelFormat24bppRGB))
			if (IsEqualGUID(formatGUID, GUID_WICPixelFormat24bppBGR))
				RGBtoBGR(data, image.size);
			else
				throw "Not supported format";

		UINT stride = (image.width * 24 + 7) / 8;

		if (FAILED(frame->WritePixels(image.height, stride, image.size, data)))
			throw "Cannot write pixels to frame";

		delete[] data;

		frame->Commit();
		encoder->Commit();

		frame->Release();
		encoder->Release();
		stream->Release();
#endif
	}

	Image3c* WICDecoder::parse(unsigned char *data, unsigned int size, unsigned int width, unsigned int height, bool toDevice) {
		Image3c* image = new Image3c();

		if (toDevice) {
			if (cudaMalloc((void**)&image->pixels, size) != cudaSuccess)
				throw "Cannot allocate memory on device";
			if (cudaMemcpy(image->pixels, data, size, cudaMemcpyHostToDevice) != cudaSuccess)
				throw "Cannot copy memory to device";
		}
		else {
			if (!(image->pixels = (pixel3c*)malloc(size)))
				throw "Cannot allocate memory on host";
			if (!(memcpy(image->pixels, data, size)))
				throw "Cannot copy memory on host";
		}

		image->onDevice = toDevice;
		image->size = size;
		image->width = width;
		image->height = height;

		return image;
	}

	void WICDecoder::RGBtoBGR(unsigned char *data, unsigned int size) {
		BYTE tmp;
		size -= 3;
		for (int i = 0; i < size; i+=3) {
			tmp = data[i];
			data[i] = data[i + 2];
			data[i + 2] = tmp;
		}
	}

}