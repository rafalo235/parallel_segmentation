#include "core.h"

#include "CPUFunctions.h"
#include "Types.h"
#include <Windows.h>

namespace parallel_seg {

	void sumRowLogIter(float *out, int width, int height, int stride);

	double imageIntegralDef(const pixel3f *in, pixel3f *out, int width, int height) {
		pixel3f sum;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int index = y * width + x;
				sum += in[index];
				out[index] = sum;
				if (y > 0)
					out[index] += out[index - width];
			}
			sum = pixel3f(0.f, 0.f, 0.f);
		}
		return 0.0;
	}

	double imageIntegralDef(const float *in, float **outPtr, int width, int height) {
		LARGE_INTEGER frequency, t1, t2;
		int widthIntegral = width + 1;
		int indexIn = 0;
		float *out = *outPtr, elapsedTime;
		int indexOut = widthIntegral + 1;
		
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);

		for (int y = 0; y < height; y++) {
			float sum = 0.f;
			for (int x = 0; x < width; x++, indexIn++, indexOut++) {
				//int indexIn = y * width + x;
				//int indexOut = (y + 1) * widthIntegral + x + 1;
				sum += in[indexIn];
				out[indexOut] = sum;
				if (y > 0)
					out[indexOut] += out[indexOut - widthIntegral];
			}
			indexOut++;
		}

		QueryPerformanceCounter(&t2);
		return (double)(t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	}
}