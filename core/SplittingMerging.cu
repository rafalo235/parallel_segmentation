#include "CPUFunctions.h"

#include "Types.h"
#include <queue>
#include <hash_map>
#include <fstream>
#include <Windows.h>
#include "GPUFunctions.h"

namespace parallel_seg {
	
	bool splitRectangle(const pixel3f* pixelsLab, const rect& rectangle, int width, int height, pixel3f& pattern) {
		bool result = false;
		int counter = 0;
		pattern = pixelsLab[rectangle.top * width + rectangle.left];
		for (int y = rectangle.top; y < rectangle.bottom; y++) {
			for (int x = rectangle.left; x < rectangle.right; x++) {
				int index = y * width + x;
				if (pixel3f::difference(pattern, pixelsLab[index]) >= HUNTER_LAB_THRESHOLD) {
					result = true;
					goto outside;
				}
				else {
					pattern = operator+(operator*(pattern, counter), pixelsLab[index]);
					counter++;
					pattern /= float(counter);
				}
			}
		}
		outside:

		return result;
	}

	bool splitRectangleIntegral(const pixel3f* pixelsLab, const pixel3f *integral, const rect& rectangle, int width, int height, pixel3f& pattern) {
		bool result = false;
		int counter = 0;
		int index = rectangle.top * width + rectangle.left;
		pattern = operator+(operator+(integral[(rectangle.bottom - 1) * width + rectangle.right - 1], pixelsLab[index]), integral[index]);
		pattern -= operator+(integral[(rectangle.bottom - 1) * width + rectangle.left - 1], integral[rectangle.top * width + rectangle.right - 1]);
		pattern /= (rectangle.bottom - rectangle.top) * (rectangle.right - rectangle.left);
		for (int y = rectangle.top; y < rectangle.bottom; y++) {
			for (int x = rectangle.left; x < rectangle.right; x++) {
				int index = y * width + x;
				if (pixel3f::difference(pattern, pixelsLab[index]) >= HUNTER_LAB_THRESHOLD) {
					result = true;
					goto outside;
				}
			}
		}
	outside:

		return result;
	}

	void fill(pixel3f *pixelsLab, const pixel3f& color, const rect& rectangle, int width, int height) {
		for (int y = rectangle.top; y < rectangle.bottom; y++) {
			for (int x = rectangle.left; x < rectangle.right; x++) {
				pixelsLab[y * width + x] = color;
			}
		}
	}

	struct elapsedTime {
		int n = 0;
		double time = 0.0;
	};

	void splitImage(const pixel3f *pixelsLab, pixel3f *out, int width, int height) {
		LARGE_INTEGER t1, t2, frequency;
		std::queue<rect> rectangles;
		std::hash_map<int, elapsedTime> map;
		pixel3f *integral = new pixel3f[width * height];
		rectangles.push(rect(0, 0, height, width));
		fill(out, BLACK_LAB, rectangles.front(), width, height);

		imageIntegralDef(pixelsLab, integral, width, height);
		
		QueryPerformanceFrequency(&frequency);

		while (!rectangles.empty()) {
			rect tmp = rectangles.front(); rectangles.pop();
			
			QueryPerformanceCounter(&t1);
			pixel3f pattern;
			if (splitRectangleIntegral(pixelsLab, integral, tmp, width, height, pattern)) {
				int centerX = (tmp.left + tmp.right) / 2;
				int centerY = (tmp.top + tmp.bottom) / 2;
				if (tmp.top != centerY && tmp.left != centerX)
					rectangles.push(rect(tmp.top, tmp.left, centerY, centerX));
				if (tmp.top != centerY && centerX != tmp.right)
					rectangles.push(rect(tmp.top, centerX, centerY, tmp.right));
				if (centerY != tmp.bottom && tmp.left != centerX)
					rectangles.push(rect(centerY, tmp.left, tmp.bottom, centerX));
				if (centerY != tmp.bottom && centerX != tmp.right)
					rectangles.push(rect(centerY, centerX, tmp.bottom, tmp.right));
			}
			else {
				pixel3f neighbourPattern = BLACK_LAB;
				double diff;
				int xLeft, xRight, yTop, yBottom;
				xLeft = tmp.left - 1; xRight = tmp.right + 1;
				yTop = tmp.top - 1; yBottom = tmp.bottom + 1;
				
				for (int x = tmp.left; x < tmp.right; x++) {
					int index;
					if (yTop >= 0) {
						index = yTop * width + x;
						if (out[index] != BLACK_LAB) {
							diff = pixel3f::difference(pattern, out[index]);
							if (diff < HUNTER_LAB_THRESHOLD
									&& pixel3f::difference(pattern, out[index]) < pixel3f::difference(pattern, neighbourPattern)) 
								neighbourPattern = out[index];
						}

					}
					if (yBottom < height) {
						index = yBottom * width + x;
						if (out[index] != BLACK_LAB) { 
							diff = pixel3f::difference(pattern, out[index]);
							if (diff < HUNTER_LAB_THRESHOLD
									&& pixel3f::difference(pattern, out[index]) < pixel3f::difference(pattern, neighbourPattern))
								neighbourPattern = out[index];
						}
					}
				}

				for (int y = tmp.top; y < tmp.bottom; y++) {
					int index;
					if (xLeft >= 0) {
						index = y * width + xLeft;
						if (out[index] != BLACK_LAB) {
							diff = pixel3f::difference(pattern, out[index]);
							if (diff < HUNTER_LAB_THRESHOLD
									&& pixel3f::difference(pattern, out[index]) < pixel3f::difference(pattern, neighbourPattern))
								neighbourPattern = out[index];
						}

					}
					if (xRight < width) {
						index = y * width + xRight;
						if (out[index] != BLACK_LAB) {
							diff = pixel3f::difference(pattern, out[index]);
							if (diff < HUNTER_LAB_THRESHOLD
									&& pixel3f::difference(pattern, out[index]) < pixel3f::difference(pattern, neighbourPattern))
								neighbourPattern = out[index];
						}
					}
				}
				if (neighbourPattern != BLACK_LAB)
					pattern = neighbourPattern;

				fill(out, pattern, tmp, width, height);
			}

			QueryPerformanceCounter(&t2);

			int key = (tmp.right - tmp.left) * (tmp.bottom - tmp.top);
			if (key != 0) {
				map[key].n++;
				map[key].time += (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
			}
		}

		std::ofstream file;
		file.open(L"D:\\Udostepniony\\cpu.csv", std::ios::out);

		for (std::hash_map<int, elapsedTime>::iterator i = map.begin();
				i != map.end();
				i++) {
			file << i->first << "\t" << i->second.time << std::endl;
		}
		file.close();

		delete[] integral;

	}

}