#include "core.h"
#include "GPUFunctions.h"

namespace parallel_seg {

	int horizontalBlockSize = 256;
	int verticalBlockSize = 256;
	integralFunction_t _integralFunction = &imageIntegralRowColT;
	int xThresholdRadius = 5;
	int yThresholdRadius = 5;
	int thresholdPercent = 50;

	/*
	 * blockSize musi by� pot�g� 2
	*/
	void setHorizontalBlockSize(int blockSize) {
		horizontalBlockSize = blockSize;
	}

	void setVerticalBlockSize(int blockSize) {
		verticalBlockSize = blockSize;
	}

	void setIntegralFunction(integralFunction_t integralFunction) {
		_integralFunction = integralFunction;
	}

	integralFunction_t getIntegralFunction(void) {
		return _integralFunction;
	}

	void setXThresholdRadius(int xRadius) {
		if (xRadius > 0)
			xThresholdRadius = xRadius;
	}

	void setYThresholdRadius(int yRadius) {
		if (yRadius > 0)
			yThresholdRadius = yRadius;
	}

	void setThresholdPercent(int percent) {
		if (percent > 0 && percent < 100)
			thresholdPercent = percent;
	}

}