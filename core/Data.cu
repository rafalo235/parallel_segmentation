#include "core.h"

#include "CPUFunctions.h"
#include <fstream>
#include <iostream>

namespace parallel_seg {

	void printFloatMatrix(const float *matrix, int width, int height);

	void saveCSVData(const wchar_t* path, const int** data, unsigned int seriesNumber, unsigned int length) {
		std::ofstream file;
		file.open(path, std::ios::out);
		for (unsigned int i = 0; i < length; i++) {
			unsigned int j = 0;
			file << data[j++][i];
			do {
				file << '\t' << data[j++][i];
			} while (j < seriesNumber);
			file << std::endl;
		}
		file.close();
	}

	void printMatrix(const pixel3f* matrix, int width, int height) {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				printf("(%3.0f,%3.0f,%3.0f)", matrix[y * width + x].x, matrix[y * width + x].y, matrix[y * width + x].z);
			}
			std::cout << std::endl;
		}
	}

	void printPixelMatrix(const pixel3c *matrix, int width, int height) {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				printf("(%3d,%3d,%3d)", matrix[y * width + x][0], matrix[y * width + x][1], matrix[y * width + x][2]);
			}
			std::cout << std::endl;
		}
	}

	void printPixelMatrix(const pixel3c *matrix, int width, int height, bool onDevice) {
		if (onDevice) {
			pixel3c *tmp = new pixel3c[width * height];
			if (cudaMemcpy(tmp, matrix, width * height * sizeof(pixel3c), cudaMemcpyDeviceToHost) != cudaSuccess) {
				delete[] tmp;
				return;
			}
			printPixelMatrix(tmp, width, height);
			delete[] tmp;
		}
		else {
			printPixelMatrix(matrix, width, height);
		}
	}

	void saveFloatMatrix(const float *matrix, int width, int height, const wchar_t* path) {
		std::ofstream file;
		file.open(path, std::ios::out);
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++, index++) {
				file << x << '\t' << y << '\t' << matrix[index] << std::endl;
			}
		}
		file.close();
	}

	void saveFloatMatrix(const float *matrix, int width, int height, const wchar_t* path, bool onDevice) {
		if (onDevice) {
			float *tmp = new float[width * height];
			if (cudaMemcpy(tmp, matrix, width * height * sizeof(float), cudaMemcpyDeviceToHost) != cudaSuccess) {
				delete[] tmp;
				return;
			}
			saveFloatMatrix(tmp, width, height, path);
			delete[] tmp;
		}
		else {
			saveFloatMatrix(matrix, width, height, path);
		}
	}

	void printFloatMatrix(const float *matrix, int width, int height, bool onDevice) {
		if (onDevice) {
			float *tmp = new float[width * height];
			if (cudaMemcpy(tmp, matrix, width * height * sizeof(float), cudaMemcpyDeviceToHost) != cudaSuccess) {
				delete[] tmp;
				return;
			}
			printFloatMatrix(tmp, width, height);
			delete[] tmp;
		}
		else {
			printFloatMatrix(matrix, width, height);
		}

	}

	void printFloatMatrix(const float *matrix, int width, int height) {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				printf("%3.0f ", matrix[y * width + x]);
			}
			std::cout << std::endl;
		}
	}

}