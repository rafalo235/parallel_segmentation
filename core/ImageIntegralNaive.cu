#include "core.h"

#include "GPUFunctions.h"
#include "Types.h"

namespace parallel_seg {

	__global__ void sumRowsNaive(const float *in, float *out, int width, int height, int stride);
	__global__ void sumRowsNaive(float *out, int width, int height, int stride);
	__global__ void sumColsNaive(float *out, int width, int height, int stride);

	__global__ void sumRowsNaive(const float *in, float *out, float *lastElements, int width, int height, int stride);
	__global__ void sumColsNaive(float *out, float *lastElements, int width, int height, int stride);

	double imageIntegralRowColNaiveOneBlock(const float *in, float **outPtr, int width, int height) {
		float elapsedTime, *out = *outPtr;
		int widthIntegral = width + 1;
		cudaEvent_t start, stop;
		dim3 block, grid;

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsNaive<<<grid, block, 2 * width * sizeof(float)>>>(in, &out[widthIntegral], width, height, widthIntegral);

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = 1;
		sumColsNaive<<<grid, block, 2 * height * sizeof(float)>>>(out, width, height, widthIntegral);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	double imageIntegralRowColNaive(const float *in, float **outPtr, int width, int height) {
		dim3 grid, block;
		cudaEvent_t start, stop;
		int widthIntegral = width + 1;
		int heightIntegral = height + 1;
		float elapsedTime, *lastElements, *out = *outPtr;
		int lastElementWidth = CEIL(width, horizontalBlockSize);
		int lastElementHeight = CEIL(height, verticalBlockSize);

		// Pomiar czasu
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);
		
		// Sumowanie wierszy

		block.x = horizontalBlockSize; block.y = 1;
		grid.x = CEIL(width, horizontalBlockSize); grid.y = height;
		if (cudaMalloc((void**)&lastElements, (lastElementWidth + 1) * height * sizeof(float)) != cudaSuccess) return 0.0;
		sumRowsNaive<<<grid, block, 2 * block.x * sizeof(float)>>>(in, &out[widthIntegral], lastElements, width, height, widthIntegral);

		block.x = CEIL(width, horizontalBlockSize) < 512 ? CEIL(width, horizontalBlockSize) : 512; block.y = 1;
		grid.x = 1; grid.y = height;
		sumRowsNaive<<<grid, block, 2 * lastElementWidth * sizeof(float)>>>(lastElements, lastElementWidth, height, lastElementWidth + 1);

		addHorizontalLastElements(lastElements, &out[widthIntegral + 1], width, height, horizontalBlockSize);
		cudaFree(lastElements);

		// Sumowanie kolumn

		block.x = 1; block.y = verticalBlockSize;
		grid.x = width; grid.y = CEIL(height, verticalBlockSize);
		if (cudaMalloc((void**)&lastElements, (lastElementHeight + 1) * width * sizeof(float)) != cudaSuccess) return 0.0;
		sumColsNaive<<<grid, block, 2 * block.y * sizeof(float)>>>(out, lastElements, width, height, widthIntegral);

		block.x = CEIL(height, verticalBlockSize) < 512 ? CEIL(height, verticalBlockSize) : 512; block.y = 1;
		grid.x = 1; grid.y = width;
		sumRowsNaive<<<grid, block, 2 * lastElementHeight * sizeof(float)>>>(lastElements, lastElementHeight, width, lastElementHeight + 1);
		
		addVerticalLastElements(lastElements, &out[widthIntegral + 1], width, height, verticalBlockSize);
		cudaFree(lastElements);

		// Koniec pomiaru
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&elapsedTime, start, stop);
		return (double)elapsedTime;
	}

	/* ~~~~~~~~ Wiersze/kolumny w jednym bloku ~~~~~~~~~~~~ */

	__global__ void sumRowsNaive(const float *in, float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		int pout = 0, pin = width;

		// Przypisanie zera do pierwszej kolumny, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.x == 0)
			out[blockIdx.y * stride] = 0.0f;
		// pominieta pierwsza kolumna
		for (int i = threadIdx.x; i < width; i += blockDim.x)
			temp[pout + i] = in[blockIdx.y * width + i];
		__syncthreads();

		for (int offset = 1; offset < width; offset <<= 1) {
			pout = width - pout;
			pin = width - pin;
			
			for (int i = threadIdx.x; i < width; i += blockDim.x) { 
				if (i >= offset)
					temp[pout + i] = temp[pin + i] + temp[pin + i - offset];
				else
					temp[pout + i] = temp[pin + i];
			}

			__syncthreads();
		}

		for (int i = threadIdx.x; i < width; i += blockDim.x)
			out[blockIdx.y * stride + i + 1] = temp[pout + i];
	}

	__global__ void sumRowsNaive(float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		int pout = 0, pin = width;

		// Przypisanie zera do pierwszej kolumny, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.x == 0)
			out[blockIdx.y * stride] = 0.0f;
		// pominieta pierwsza kolumna
		for (int i = threadIdx.x; i < width; i += blockDim.x)
			temp[pout + i] = out[blockIdx.y * stride + i + 1];
		__syncthreads();

		for (int offset = 1; offset < width; offset <<= 1) {
			pout = width - pout;
			pin = width - pin;

			for (int i = threadIdx.x; i < width; i += blockDim.x) {
				if (i >= offset)
					temp[pout + i] = temp[pin + i] + temp[pin + i - offset];
				else
					temp[pout + i] = temp[pin + i];
			}

			__syncthreads();
		}

		for (int i = threadIdx.x; i < width; i += blockDim.x)
			out[blockIdx.y * stride + i + 1] = temp[pout + i];
	}

	__global__ void sumColsNaive(float *out, int width, int height, int stride) {
		extern __shared__ float temp[];
		int pout = 0, pin = height;
#if __CUDA_ARCH__ < 200
		const int offset = resolveOffset(blockIdx.x, width);
#else
		const int offset = blockIdx.x;
#endif

		// Przypisanie zera do pierwszego wiersza, kopiowanie do pamieci wspoldzielonej
		if (threadIdx.y == 0) {
			out[offset + 1] = 0.0f;
			if (offset == 0)
				out[0] = 0.0f;
		}
		// pominieta pierwsza kolumna
		for (int i = threadIdx.y; i < height; i += blockDim.y)
			temp[pout + i] = out[(i + 1) * stride + offset + 1];
		__syncthreads();

		for (int offset = 1; offset < height; offset <<= 1) {
			pout = height - pout;
			pin = height - pin;

			for (int i = threadIdx.y; i < height; i += blockDim.y) {
				if (i >= offset)
					temp[pout + i] = temp[pin + i] + temp[pin + i - offset];
				else
					temp[pout + i] = temp[pin + i];
			}

			__syncthreads();
		}

		for (int i = threadIdx.y; i < height; i += blockDim.y)
			out[(i + 1) * stride + offset + 1] = temp[pout + i];
	}

	/* ~~~~~~~~ Wiersze/kolumny rozdzielone na bloki ~~~~~~~~~~~~ */

	__global__ void sumRowsNaive(const float *__restrict__ in, float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride) {
		extern __shared__ float temp[];
		int blockOffset = blockIdx.x * blockDim.x;
		int pout = 0, pin = blockDim.x;

		if (threadIdx.x == 0 && blockIdx.x == 0) // przekaza� wska�nik na drugi wiersz
			out[blockIdx.y * stride] = 0.0f;

		if (blockOffset + threadIdx.x < width)
			temp[pout + threadIdx.x] = in[blockIdx.y * width + blockOffset + threadIdx.x];
		else
			temp[pout + threadIdx.x] = 0.0f;
		
		__syncthreads();

		for (int offset = 1; offset < blockDim.x; offset <<= 1) {
			pout = blockDim.x - pout;
			pin = blockDim.x - pin;

			if (threadIdx.x >= offset)
				temp[pout + threadIdx.x] = temp[pin + threadIdx.x] + temp[pin + threadIdx.x - offset];
			else
				temp[pout + threadIdx.x] = temp[pin + threadIdx.x];

			__syncthreads();
		}

		if (blockOffset + threadIdx.x < width)
			out[blockIdx.y * stride + blockOffset + threadIdx.x + 1] = temp[pout + threadIdx.x];
		if (threadIdx.x == blockDim.x - 1) {
			lastElements[(gridDim.x + 1) * blockIdx.y + blockIdx.x + 1] = temp[pout + threadIdx.x]; //zerowane [x][0] przy obliczaniu sumy
		}
	}

	__global__ void sumColsNaive(float *__restrict__ out, float *__restrict__ lastElements, int width, int height, int stride) {
		extern __shared__ float temp[];
#if __CUDA_ARCH__ < 200
		const int xOffset = resolveOffset(blockIdx.x, width);
#else
		const int xOffset = blockIdx.x;
#endif
		int blockOffset = blockIdx.y * blockDim.y;
		int pout = 0, pin = blockDim.y;
		
		// Zerowanie pierwszego wiersza
		if (threadIdx.y == 0 && blockIdx.y == 0) {
			out[xOffset + 1] = 0.0f;
			if (xOffset == 0)
				out[0] = 0.0f;
		}

		if (blockOffset + threadIdx.y < height)
			temp[pout + threadIdx.y] = out[(blockOffset + threadIdx.y + 1) * stride + xOffset + 1];
		else
			temp[pout + threadIdx.y] = 0.0f;

		__syncthreads();

		for (int offset = 1; offset < blockDim.y; offset <<= 1) {
			pout = blockDim.y - pout;
			pin = blockDim.y - pin;

			if (threadIdx.y >= offset)
				temp[pout + threadIdx.y] = temp[pin + threadIdx.y] + temp[pin + threadIdx.y - offset];
			else
				temp[pout + threadIdx.y] = temp[pin + threadIdx.y];

			__syncthreads();
		}
		
		if (blockOffset + threadIdx.y < height)
			out[(blockOffset + threadIdx.y + 1) * stride + xOffset + 1] = temp[pout + threadIdx.y];
		if (threadIdx.y == blockDim.y - 1) // transpozycja macierzy z sumami
			lastElements[(gridDim.y + 1) * xOffset + blockIdx.y + 1] = temp[pout + threadIdx.y];
	}

}