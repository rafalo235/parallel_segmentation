#include "ColorCreator.h"

namespace parallel_seg {

	pixel3c ColorCreator::getColor() const {
		return pixel3c(rand() % (MAX_COLOR_VAL - 1), rand() % (MAX_COLOR_VAL - 1), rand() % (MAX_COLOR_VAL - 1));
	}

}