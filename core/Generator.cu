#include "core.h"

#include <cuda.h>
#include "IDecoder.h"
#include "Types.h"
#include "GPUFunctions.h"
#include <exception>

#define TILE_DIM	32
#define BLOCK_ROWS	8

namespace parallel_seg {

	__global__ void fill(pixel3c *out, int width, int height, pixel3c value);
	__global__ void generateSequenceOnDevice(float *out, int width, int height);
	__global__ void generateSequenceOnDevice(pixel3c *out, int width, int height);
	void generateSequenceOnHost(float *out, int size);

	float *generateFloatSequence(int width, int height, bool onDevice) {
		float *out;
		int size = width * height;
		if (onDevice) {
			dim3 block(TILE_DIM, BLOCK_ROWS), grid;
			if (cudaMalloc((void**)&out, size * sizeof(float)) != cudaSuccess)
				throw std::bad_alloc();
			grid.x = CEIL(width, TILE_DIM); grid.y = CEIL(height, TILE_DIM);
			generateSequenceOnDevice<<<grid, block>>>(out, width, height);
		}
		else {
			if ((out = (float*)malloc(size * sizeof(float))) == nullptr)
				throw std::bad_alloc();
			generateSequenceOnHost(out, size);
		}
		return out;
	}

	float *generateFloat(int width, int height, bool onDevice) {
		float *out;
		int size = width * height;
		if (onDevice) {
			if (cudaMalloc((void**)&out, size * sizeof(float)) != cudaSuccess)
				throw std::bad_alloc();
		}
		else {
			if ((out = (float*)malloc(size * sizeof(float))) == nullptr)
				throw std::bad_alloc();
		}
		return out;
	}

	Image3c *generateImageSequence(int width, int height) {
		int size = width * height;
		Image3c *out = new Image3c();
		out->pixels = new pixel3c[size];
		out->width = width; out->height = height;
		out->size = size * sizeof(pixel3c);
		out->onDevice = false;
		for (int i = 0; i < size; i++) {
			out->pixels[i] = pixel3c((float)(i % MAX_COLOR_VAL));
		}
		return out;
	}

	Image3c *generateImage(int width, int height, bool onDevice) {
		return generateImage(width, height, onDevice, pixel3c(0, 0, 0));
	}

	Image3c *generateImage(int width, int height, bool onDevice, pixel3c value) {
		int size = width * height;
		Image3c *out = new Image3c();
		out->width = width; out->height = height;
		out->size = size * sizeof(pixel3c);
		out->onDevice = onDevice;
		if (onDevice) {
			dim3 block(TILE_DIM, BLOCK_ROWS), grid;
			if (cudaMalloc((void**)&out->pixels, size * sizeof(float)) != cudaSuccess)
				throw std::bad_alloc();
			grid.x = CEIL(width, TILE_DIM); grid.y = CEIL(height, TILE_DIM);
			fill<<<grid, block>>>(out->pixels, width, height, value);
		}
		else {
			if ((out->pixels = (pixel3c*)malloc(size * sizeof(pixel3c))) == nullptr)
				throw std::bad_alloc();
			for (int i = 0; i < size; i++) {
				out->pixels[i] = value;
			}
		}
		return out;
	}


	__global__ void generateSequenceOnDevice(float *out, int width, int height) {
		int x = blockIdx.x * TILE_DIM + threadIdx.x;
		int y = blockIdx.y * TILE_DIM + threadIdx.y;
		if (x < width) {
			const int end = (y + TILE_DIM) < height ? (y + TILE_DIM) : height;
			for (; y < end; y += BLOCK_ROWS) {
				int index = y * width + x;
				out[index] = index;
			}
		}
	}

	__global__ void generateSequenceOnDevice(pixel3c *out, int width, int height) {
		int x = blockIdx.x * TILE_DIM + threadIdx.x;
		int y = blockIdx.y * TILE_DIM + threadIdx.y;
		if (x < width) {
			const int end = (y + TILE_DIM) < height ? (y + TILE_DIM) : height;
			for (; y < end; y += BLOCK_ROWS) {
				int index = y * width + x;
				out[index] = pixel3c(index % MAX_COLOR_VAL);
			}
		}
	}

	__global__ void fill(pixel3c *out, int width, int height, pixel3c value) {
		int x = blockIdx.x * TILE_DIM + threadIdx.x;
		int y = blockIdx.y * TILE_DIM + threadIdx.y;
		if (x < width) {
			const int end = (y + TILE_DIM) < height ? (y + TILE_DIM) : height;
			for (; y < end; y += BLOCK_ROWS) {
				out[y * width + x] = value;
			}
		}
	}

	void generateSequenceOnHost(float *out, int size) {
		for (int i = 0; i < size; i++) {
			out[i] = (float)i;
		}
	}

}