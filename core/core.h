#ifndef INC_CORE_H
#define INC_CORE_H

#ifdef PARALLEL_SEGMENTATION_DLL_EXPORTS
#define PARALLEL_SEGMENTATION_API __declspec(dllexport) 
#else
#define PARALLEL_SEGMENTATION_API __declspec(dllimport) 
#endif

namespace parallel_seg {

	struct pixel3c;
	class Image3c;

	typedef double (*integralFunction_t)(const float*, float**, int, int);

	// Parametry przetwarzania
	PARALLEL_SEGMENTATION_API void setHorizontalBlockSize(int blockSize);
	PARALLEL_SEGMENTATION_API void setVerticalBlockSize(int blockSize);
	PARALLEL_SEGMENTATION_API void setIntegralFunction(integralFunction_t integralFunction);
	PARALLEL_SEGMENTATION_API integralFunction_t getIntegralFunction(void);
	PARALLEL_SEGMENTATION_API void setXThresholdRadius(int xRadius);
	PARALLEL_SEGMENTATION_API void setYThresholdRadius(int yRadius);
	PARALLEL_SEGMENTATION_API void setThresholdPercent(int percent);
	
	PARALLEL_SEGMENTATION_API void release(void* pointer, bool onDevice);
	PARALLEL_SEGMENTATION_API void finish();

	// Tworzenie danych testowych
	PARALLEL_SEGMENTATION_API float *generateFloat(int width, int height, bool onDevice);
	PARALLEL_SEGMENTATION_API float *generateFloatSequence(int width, int height, bool onDevice);
	PARALLEL_SEGMENTATION_API Image3c *generateImage(int width, int height, bool onDevice);
	PARALLEL_SEGMENTATION_API Image3c *generateImage(int width, int height, bool onDevice, pixel3c value);
	PARALLEL_SEGMENTATION_API Image3c *generateImageSequence(int width, int height);

	//// Obraz ca�kowy
	// CPU
	PARALLEL_SEGMENTATION_API double imageIntegralDef(const float *in, float **out, int width, int height);
	// GPU
	PARALLEL_SEGMENTATION_API double imageIntegralRowColT(const float* in, float **out, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowCol(const float* in, float **out, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColEfficientOneBlock(const float *in, float **outPtr, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColEfficient(const float *in, float **outPtr, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColBasketOneBlock(const float *in, float **outPtr, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColBasket(const float *in, float **outPtr, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColNaiveOneBlock(const float *in, float **outPtr, int width, int height);
	PARALLEL_SEGMENTATION_API double imageIntegralRowColNaive(const float *in, float **out, int width, int height);

	//// Jasno�� obrazu
	PARALLEL_SEGMENTATION_API void getImageLuminance(const pixel3c *in, float *out, int width, int height, bool onDevice);
	
	//// Binaryzacja
	PARALLEL_SEGMENTATION_API double gpuAdaptiveThreasholding(const pixel3c *in, pixel3c *out, int width, int height);
	PARALLEL_SEGMENTATION_API double gpucpuAdaptiveThresholding(const pixel3c *in, pixel3c *out, int width, int height);

	//// Segmentacja
	PARALLEL_SEGMENTATION_API void cpuHistogramSegmentation(pixel3c *in, pixel3c *out, unsigned int width, unsigned int height);
	PARALLEL_SEGMENTATION_API void cpuRegionGrowingSegmentation(pixel3c *in, pixel3c *out, unsigned int width, unsigned int height);
	PARALLEL_SEGMENTATION_API void cpuRegionSplittingSegmentation(const pixel3c *in, pixel3c *out, unsigned int width, unsigned int height);
	
	//// Wy�wietlanie danych testowych
	PARALLEL_SEGMENTATION_API void printFloatMatrix(const float *matrix, int width, int height, bool onDevice);
	PARALLEL_SEGMENTATION_API void saveFloatMatrix(const float *matrix, int width, int height, const wchar_t* path, bool onDevice);
	PARALLEL_SEGMENTATION_API void printPixelMatrix(const pixel3c *matrix, int width, int height, bool onDevice);
}

#endif
