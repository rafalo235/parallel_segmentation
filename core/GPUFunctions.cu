#include "core.h"
#include "GPUFunctions.h"
#include "Types.h"
#include <iostream>
#include "CPUFunctions.h"

namespace parallel_seg {

	void finish() {
		cudaDeviceReset();
	}

	cudaError_t reallocMatrix(float **out, int *width, int *height, int *widthIntegral, int *heightIntegral, int horizontalBlockSize, int verticalBlockSize) {
		if ((*widthIntegral % horizontalBlockSize == 1) && (*heightIntegral % verticalBlockSize == 1)) return cudaSuccess;
		cudaFree(*out);
		if (*widthIntegral % horizontalBlockSize != 1) {
			*widthIntegral = (*widthIntegral / horizontalBlockSize + 1) * horizontalBlockSize + 1;
			*width = *widthIntegral - 1;
		}
		if (*heightIntegral % verticalBlockSize != 1) {
			*heightIntegral = (*heightIntegral / verticalBlockSize + 1) * verticalBlockSize + 1;
			*height = *heightIntegral - 1;
		}
		return cudaMalloc((void**)out, (*widthIntegral) * (*heightIntegral) * sizeof(float));
	}

	void release(void* pointer, bool onDevice) {
		if (onDevice)
			cudaFree(pointer);
		else
			delete[] pointer;
	}

}