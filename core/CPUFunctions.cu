#include "core.h"
#include "CPUFunctions.h"

#include "ColorCreator.h"
#include "LuminanceHistogram.h"

#include <stack>
#include <iostream>
#include <Windows.h>

namespace parallel_seg {

	void cpuSplittingAndMerging(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int wend, unsigned int hend, unsigned int stride, const ColorCreator& creator) {
		int start = h * stride + w;
		static pixel3c color;
		color = out[start];
		if (out[start] == BLACK)
			color = creator.getColor();
		for (int j = h; j < hend; j++) {
			for (int i = w; i < wend; i++) {
				int index = j * stride + i;
				if (out[index] != BLACK)
					continue;
				if (pixel3c::difference(in[start], in[index]) > 20.0) {
					unsigned int wmiddle = (wend + w) >> 1;
					unsigned int hmiddle = (hend + h) >> 1;
					cpuSplittingAndMerging(in, out, wmiddle, hmiddle, wend, hend, stride, creator);
					cpuSplittingAndMerging(in, out, w, hmiddle, wmiddle, hend, stride, creator);
					cpuSplittingAndMerging(in, out, wmiddle, h, wend, hmiddle, stride, creator);
					cpuSplittingAndMerging(in, out, w, h, wmiddle, hmiddle, stride, creator);
					break;
				}
				out[index] = color;
			}
		}
	}

	void indexRecursively(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int x, unsigned int y, const pixel3c& referencevalue) {
		if (x < 0 || y < 0 || x >= w || y >= h)
			return;
		int index = x + y * w;
		if (out[index] != BLACK)
			return;
		if (pixel3c::difference(in[index], referencevalue) < THRESHOLD) {
			out[index] = referencevalue;
			indexRecursively(in, out, w, h, x, y - 1, referencevalue);
			indexRecursively(in, out, w, h, x + 1, y, referencevalue);
			indexRecursively(in, out, w, h, x, y + 1, referencevalue);
			indexRecursively(in, out, w, h, x - 1, y, referencevalue);
		}
	}

	void indexSequentailly(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int sx, unsigned int sy) {
		int size = w * h;
		bool *checked = new bool[size];
		for (int i = size - 1; i >= 0; i--) checked[i] = false;
		pixel3c pattern = in[sx + sy * w];
		std::stack<int> pixels;
		pixels.push(sx + sy * w);
		while (!pixels.empty()) {
			int index = pixels.top(); pixels.pop();
			checked[index] = true;
			if (pixel3c::difference(pattern, in[index]) < THRESHOLD) {
				out[index] = pattern;
				
				int up = index - w;
				int down = index + w;
				int left = index - 1;
				int right = index + 1;

				if (up >= 0 && !checked[up])
					pixels.push(up);

				if (down < size && !checked[down])
					pixels.push(down);

				if (left >= 0 && !checked[left])
					pixels.push(left);

				if (right < size && !checked[right])
					pixels.push(right);
			}
		}

		delete[] checked;
	}

	

	void cpuRegionGrowing(pixel3c* in, pixel3c* out, unsigned int w, unsigned int h, unsigned int x, unsigned int y) {
		indexRecursively(in, out, w, h, x, y, in[x + y * w]);
	}

	void cpuCreateLuminanceHistogram(const pixel3c* in, unsigned int size, LuminanceHistogram& histogram) {
		for (size--; size > 0; size--)
			histogram.add(in[size]);
		histogram.add(in[0]);
	}

	static inline pixel3c cpuAssignSegment(const pixel3c& pixel, const std::list<segment> &segments) {
		pixel3c c(0, 0, 0);
		std::list<segment>::const_iterator i;
		float lum = pixel.luminance();


		for (i = segments.begin(); i != segments.end(); i++) {
			if (lum >= (*i).left && lum < (*i).right) {
				c[0] = c[1] = c[2] = (*i).max > MAX_COLOR_VAL ? MAX_COLOR_VAL : (*i).max;
				break;
			}
		}
		if (i == segments.end()) {
			c[0] = c[1] = c[2] = 0;
		}

		return c;
	}

	void cpuHistogramSegmentation(pixel3c *in, pixel3c *out, unsigned int width, unsigned int height) {
		unsigned int size = width * height;
		const int MAX_SEGMENT = 20;
		int times = 0x4;
		
		LuminanceHistogram lh;
		std::list<segment> *segments;

		cpuCreateLuminanceHistogram(in, size, lh);
		segments = lh.getSegments();
		
		for (unsigned int i = 0; i < size; i++) {
			out[i] = cpuAssignSegment(in[i], *segments);
		}
		delete segments;
	}

	int* derivate(const int* data, unsigned int length) {
		int *derivative = new int[length];
		int i;
		for (i = 0; i < length - 1; i++)
			derivative[i] = data[i + 1] - data[i];
		derivative[i] = derivative[i - 1];
		return derivative;
	}

	void filterMean(int* data, unsigned int length, unsigned short windowRadius) {
		int mean, i, c;
		for (int i = 0; i < length; i++) {
			mean = data[i];
			c = 1;
			for (int j = 1; j <= windowRadius; j++) {
				if (i + j < length) {
					mean += data[i + j];
					c++;
				}
				if (i - j >= 0) {
					mean += data[i - j];
					c++;
				}
			}
			data[i] = mean / c;
		}
	}

	void filterMedian(int* data, unsigned int length, unsigned short windowRadius) {
		sortedTable_t tab(windowRadius * 2 + 1);
		circularList_t copies(windowRadius + 2);

		for (int i = 0; i < windowRadius && i < length; i++) {
			tab.add(data[i]);
		}
		for (int i = 0; i < length; i++) {
			if (i - windowRadius - 1 >= 0)
				tab.remove(copies[0]);
			if (i + windowRadius < length)
				tab.add(data[i + windowRadius]);
			copies.add(data[i]);
			data[i] = tab.median();
		}
	}

	static bool isZeroPoint(const int *data, int index, unsigned int length) {
		if (index < length - 1) {
			return data[index] == 0 || data[index] * data[index + 1] < 0;
		}
		return data[index] == 0;
	}

	int* getZeroPoints(const int *data, unsigned int length, int &counter) {
		int* zeroPoints = (int*)malloc(length * sizeof(int));
		counter = 0;
		for (int i = 0; i < length - 1; i++) {
			if (isZeroPoint(data, i, length) && !isZeroPoint(data, i + 1, length)) {
				zeroPoints[counter++] = i;
			}
		}
		if (data[length - 1] == 0)
			zeroPoints[counter++] = length - 1;
		// Obszar zmniejszany (zawsze sie powiedzie)
		zeroPoints = (int*)realloc(zeroPoints, counter * sizeof(int));
		return zeroPoints;
	}

	int* getIndices(unsigned int length) {
		int* indices = new int[length];
		for (int i = 0; i < length; i++) {
			indices[i] = i;
		}
		return indices;
	}

	void cpuRegionGrowingSegmentation(pixel3c *in, pixel3c *out, unsigned int width, unsigned int height) {
		for (int i = 0; i < width * height; i++) {
			pixel3f f = pixel3f::xyzToHunterLab(pixel3f::rgbToXyz(in[i]));
			
			out[i] = pixel3c::xyzToRgb(pixel3f::hunterLabToXyz(f));
		}
	}


	void cpuRegionSplittingSegmentation(const pixel3c *in, pixel3c *out, unsigned int width, unsigned int height) {

		pixel3f *pixelsIn = new pixel3f[width * height],
			*pixelsOut = new pixel3f[width* height];

		for (int i = 0; i < width * height; i++) {
			pixelsIn[i] = pixel3f::xyzToHunterLab(pixel3f::rgbToXyz(in[i]));
		}

		LARGE_INTEGER t1, t2, frequency;

		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);

		splitImage(pixelsIn, pixelsOut, (int)width, (int)height);

		QueryPerformanceCounter(&t2);

		std::wcout << L"Czas na CPU: " << ((t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart) << std::endl;


		for (int i = 0; i < width * height; i++) {
			out[i] = pixel3c::xyzToRgb(pixel3f::hunterLabToXyz(pixelsOut[i]));
		}
	}

}