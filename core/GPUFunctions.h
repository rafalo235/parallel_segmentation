#pragma once

#include <cuda.h>

#define CEIL(x,y) (((x) + (y) - 1) / (y))

namespace parallel_seg {

	struct pixel3c;
	union pixel3f;

	extern int horizontalBlockSize;
	extern int verticalBlockSize;

	void copyToIntegral(const float *in, float *out, int width, int height);
	void transposeDiagonal(const float *in, float *out, int width, int height);
	
	// Konwersja
	__global__ void rgbToHunterLabGPU(const pixel3c *in, pixel3f *out, int size);
	__global__ void hunterLabToRgbGPU(const pixel3f *in, pixel3c *out, int size);

	void convertToLuminance(const pixel3c *in, float *out, int width, int height);
	
	__device__ int resolveOffset(int block, int width);

	void addHorizontalLastElements(const float *in, float *out, int widthOut, int heightOut, int horizontalBlock);
	void addVerticalLastElements(const float *in, float *out, int widthOut, int heightOut, int verticalBlock);

	cudaError_t reallocMatrix(float **out, int *width, int *height, int *widthIntegral, int *heightIntegral, int horizontalBlockSize, int verticalBlockSize);

}