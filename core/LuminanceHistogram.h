#pragma once

#include "Types.h"

#include <list>

namespace parallel_seg {

	struct segment {
		int left, right, max;
	};

	class LuminanceHistogram {
	private:
		int *tab = nullptr;
		unsigned int length;
	public:
		LuminanceHistogram();
		~LuminanceHistogram();

		void add(const pixel3c& pixel);
		void save(const wchar_t* path) const;
		void getLocalExtrema(std::list<channel> &minimas, std::list<channel> &maximas) const;
		void smooth();
		std::list<segment> *getSegments() const;
		static void printSegments(std::list<segment> &segments);
	};

}