#include "IDecoder.h"

#ifdef PARALLEL_SEGMENTATION_DLL_EXPORTS
#define PARALLEL_SEGMENTATION_API __declspec(dllexport) 
#else
#define PARALLEL_SEGMENTATION_API __declspec(dllimport) 
#endif

namespace parallel_seg {

	class FreeImageDecoder : public IDecoder {
	public:
		PARALLEL_SEGMENTATION_API void initialize(void);
		PARALLEL_SEGMENTATION_API void uninitialize(void);
		PARALLEL_SEGMENTATION_API Image3c* decode(wchar_t *path, bool toDevice);
		PARALLEL_SEGMENTATION_API void encode(const Image3c& image, wchar_t *path);

		PARALLEL_SEGMENTATION_API static const char* getCopyright(void);
	};

}