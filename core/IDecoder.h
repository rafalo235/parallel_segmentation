#pragma once

#ifdef PARALLEL_SEGMENTATION_DLL_EXPORTS
#define PARALLEL_SEGMENTATION_API __declspec(dllexport) 
#else
#define PARALLEL_SEGMENTATION_API __declspec(dllimport) 
#endif

namespace parallel_seg {

	struct pixel3c;

	class Image {

	};

	class Image3c : public Image {
	public:
		bool onDevice;
		pixel3c *pixels;
		unsigned int size;
		unsigned int width, height;

		PARALLEL_SEGMENTATION_API Image3c() { };
		PARALLEL_SEGMENTATION_API ~Image3c();
		PARALLEL_SEGMENTATION_API Image3c* copy(bool toDevice) const;
	};

	class IDecoder {
	public:
		PARALLEL_SEGMENTATION_API virtual void initialize(void) = 0;
		PARALLEL_SEGMENTATION_API virtual void uninitialize(void) = 0;
		PARALLEL_SEGMENTATION_API virtual Image3c* decode(wchar_t *path, bool toDevice) = 0;
		PARALLEL_SEGMENTATION_API virtual void encode(const Image3c& image, wchar_t *path) = 0;
	};

}