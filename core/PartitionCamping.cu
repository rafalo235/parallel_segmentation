#include "GPUFunctions.h"

namespace parallel_seg {

	// 256 byte / 4 byte(float) = 64
#define PARTITION_WIDTH		64
#define LOG_PARTITION_WIDTH 6

	__device__ int resolveOffset(int block, int width) {
		//int grd = (width / PARTITION_WIDTH) * PARTITION_WIDTH;
		int grd = width - (width % PARTITION_WIDTH);
		int j = block;
		if (block < grd) {
			// j *= PARTITION_WIDTH
			j <<= LOG_PARTITION_WIDTH;
			int offset = j / grd;
			j = (j % grd) + offset;
		}
		return j;
	}

}