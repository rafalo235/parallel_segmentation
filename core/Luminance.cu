#include "core.h"
#include "GPUFunctions.h"
#include "CPUFunctions.h"
#include "Types.h"

#define TILE_DIM			32
#define BLOCK_ROWS			8

namespace parallel_seg {

	__global__ void getLuminance(const pixel3c *in, float *out, int width, int height);
	void cpuGetLuminance(const pixel3c *in, float *out, int size);

	void getImageLuminance(const pixel3c *in, float *out, int width, int height, bool onDevice) {
		if (onDevice)
			convertToLuminance(in, out, width, height);
		else
			cpuGetLuminance(in, out, width * height);
	}

	void convertToLuminance(const pixel3c *in, float *out, int width, int height) {
		dim3 block(TILE_DIM, BLOCK_ROWS), grid;
		grid.x = CEIL(width, TILE_DIM); grid.y = CEIL(height, TILE_DIM);
		getLuminance<<<grid, block>>>(in, out, width, height);
	}

	__global__ void getLuminance(const pixel3c *in, float *out, int width, int height) {
		int x = blockIdx.x * TILE_DIM + threadIdx.x;
		int y = blockIdx.y * TILE_DIM + threadIdx.y;
		int end = height > y + TILE_DIM ? y + TILE_DIM : height;

		if (x < width)
			for (int i = y; i < end; i += BLOCK_ROWS)
				out[i * width + x] = in[i * width + x].luminance();
	}

	void cpuGetLuminance(const pixel3c *in, float *out, int size) {
		size--;
		for (; size >= 0; --size) {
			out[size] = in[size].luminance();
		}
	}

}