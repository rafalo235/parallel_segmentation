#include "core.h"

#include "Types.h"

#include <cuda.h>
#include <fstream>
#include <Windows.h>
#include "GPUFunctions.h"

namespace parallel_seg {

	__device__ int mutex;

	__global__ void splitRectangleGPU(const pixel3f *in, pixel3f *out, rect frame, int width, int height);
	__global__ void splitRectangleIntegralGPU(const pixel3f *in, const pixel3f *integral, pixel3f *out, rect frame, int width, int height);
	__device__ pixel3c convertXyzToRgb(pixel3f xyz);
	__device__ pixel3f convertRgbToXyz(const pixel3c& rgb);
	__device__ pixel3f convertXyzToHunterLab(const pixel3f& xyz);
	__device__ pixel3f convertHunterLabToXyz(const pixel3f& xyz);
	__device__ double differenceF(const pixel3f &a, const pixel3f &b);
	__device__ pixel3f getNeighbourPattern(const pixel3f *in, const pixel3f &color, const rect &frame, int width, int height, int x, int y);
	__global__ void setMutex(int value);

	__global__ void rgbToHunterLabGPU(const pixel3c *in, pixel3f *out, int size) {
		int x = (blockIdx.x * blockDim.x) + threadIdx.x;
		if (x < size)
			out[x] = convertXyzToHunterLab(convertRgbToXyz(in[x]));
	}

	__global__ void hunterLabToRgbGPU(const pixel3f *in, pixel3c *out, int size) {
		int x = (blockIdx.x * blockDim.x) + threadIdx.x;
		if (x < size)
			out[x] = convertXyzToRgb(convertHunterLabToXyz(in[x]));
	}

	__global__ void fillGPU(pixel3f *in, pixel3f color, int size) {
		int x = (blockIdx.x * blockDim.x) + threadIdx.x;
		if (x < size)
			in[x] = color;
	}

	__global__ void splitRectangleGPU(const pixel3f *in, pixel3f *out, rect frame, int width, int height) {
		const pixel3f _black(0.f, 0.f, 0.f);

		int x = ((blockIdx.x * blockDim.x) + threadIdx.x) * frame.right;
		int y = ((blockIdx.y * blockDim.y) + threadIdx.y) * frame.bottom;
		int right = x + frame.right;
		int bottom = y + frame.bottom;
		int index = y * width + x;
		pixel3f pattern = in[index];
		int counter = 0;
		bool split = false;

		if (out[index] == _black) {

			for (int yy = y; yy < bottom; yy++) {
				for (int xx = x; xx < right; xx++) {
					if (differenceF(in[index], pattern) < HUNTER_LAB_THRESHOLD) {
						pattern = pattern * counter + in[index];
						counter++;
						pattern /= counter;
					} else {
						split = true;
						goto outside;
					}
					index++;
				}
				index += width - frame.right;
			}

			/*pixel3f nPat = getNeighbourPattern(out, pattern, frame, width, height, x, y);
			if (nPat != _black)
				pattern = nPat;*/
		}
	outside:
		//__syncthreads();

		index = y * width + x;
		if (!split && out[index] == _black) {

			for (int yy = y; yy < bottom; yy++) {
				for (int xx = x; xx < right; xx++) {
					out[index] = pattern;
					index++;
				}
				index += width - frame.right;
			}

		}
		//__syncthreads();
	}

	__global__ void splitRectangleIntegralGPU(const pixel3f *in, const pixel3f *integral, pixel3f *out, rect frame, int width, int height) {

		const pixel3f _black(0.f, 0.f, 0.f);

		int x = ((blockIdx.x * blockDim.x) + threadIdx.x) * frame.right;
		int y = ((blockIdx.y * blockDim.y) + threadIdx.y) * frame.bottom;

		if (x < width && y < height) {

			int right = x + frame.right;
			int bottom = y + frame.bottom;
			int index = y * width + x;
			pixel3f pattern = integral[bottom * width + right] + integral[index] - integral[bottom * width + x] - integral[y * width + right];
			pattern /= frame.right * frame.bottom;
			bool split = false;

			if (out[index] == _black) {

				for (int yy = y; yy < bottom; yy++) {
					for (int xx = x; xx < right; xx++) {
						if (differenceF(in[index], pattern) >= HUNTER_LAB_THRESHOLD) {
							split = true;
							goto outside;
						}
						index++;
					}
					index += width - frame.right;
				}

				pixel3f nPat = getNeighbourPattern(out, pattern, frame, width, height, x, y);
				if (nPat != _black)
				pattern = nPat;
			}
		outside:
			//__syncthreads();

			index = y * width + x;
			if (!split && out[index] == _black) {

				for (int yy = y; yy < bottom; yy++) {
					for (int xx = x; xx < right; xx++) {
						out[index] = pattern;
						index++;
					}
					index += width - frame.right;
				}

			}
		}
		//__syncthreads();
	}

	__device__ pixel3c convertXyzToRgb(pixel3f xyz) {
		float r, g, b;
		xyz.x /= 100.f;
		xyz.y /= 100.f;
		xyz.z /= 100.f;

		r = xyz.x *  3.2406f + xyz.y * -1.5372f + xyz.z * -0.4986f;
		g = xyz.x * -0.9689f + xyz.y *  1.8758f + xyz.z *  0.0415f;
		b = xyz.x *  0.0557f + xyz.y * -0.2040f + xyz.z *  1.0570f;

		if (r > 0.0031308f)		r = 1.055f * powf(r, 1.f / 2.4f) - 0.055f;
		else					r = 12.92f * r;
		if (g > 0.0031308f)		g = 1.055f * powf(g, 1.f / 2.4f) - 0.055f;
		else					g = 12.92f * g;
		if (b > 0.0031308f)		b = 1.055f * powf(b, 1.f / 2.4f) - 0.055f;
		else					b = 12.92f * b;

		return pixel3c(
			(channel)(r * 255.f),
			(channel)(g * 255.f),
			(channel)(b * 255.f)
			);
	}
	__device__ pixel3f convertRgbToXyz(const pixel3c& rgb) {
		float r = (float)rgb.values[0] / MAX_COLOR_VAL;
		float g = (float)rgb.values[1] / MAX_COLOR_VAL;
		float b = (float)rgb.values[2] / MAX_COLOR_VAL;

		if (r > 0.04045f)		r = powf(((r + 0.055f) / 1.055f), 2.4f);
		else					r = r / 12.92f;
		if (g > 0.04045f)		g = powf(((g + 0.055f) / 1.055f), 2.4f);
		else					g = g / 12.92f;
		if (b > 0.04045f)		b = powf(((b + 0.055f) / 1.055f), 2.4f);
		else					b = b / 12.92f;

		r *= 100.f; g *= 100.f; b *= 100.f;

		return pixel3f(
			r * 0.4124f + g * 0.3576f + b * 0.1805f,
			r * 0.2126f + g * 0.7152f + b * 0.0722f,
			r * 0.0193f + g * 0.1192f + b * 0.9505f
			);
	}
	__device__ pixel3f convertXyzToHunterLab(const pixel3f& xyz) {
		return pixel3f(
			10.f * sqrtf(xyz.y),
			17.5f * (((1.02f * xyz.x) - xyz.y) / sqrtf(xyz.y)),
			7.f * ((xyz.y - (0.847f * xyz.z)) / sqrtf(xyz.y))
			);
	}
	__device__ pixel3f convertHunterLabToXyz(const pixel3f& lab) {
		pixel3f xyz;
		float y = lab.l / 10.f;
		float x = lab.a / 17.5f * lab.l / 10.f;
		float z = lab.b / 7.f * lab.l / 10.f;

		xyz.y = powf(y, 2.f);
		xyz.x = (x + xyz.y) / 1.02f;
		xyz.z = -(z - xyz.y) / 0.847f;

		return xyz;
	}

	__device__ double differenceF(const pixel3f &a, const pixel3f &b) {
		pixel3f c = operator-(a, b);
		return sqrt((double)(c.values[0] * c.values[0] + c.values[1] * c.values[1] + c.values[2] * c.values[2]));
	}

	__device__ pixel3f getNeighbourPattern(const pixel3f *in, const pixel3f &color, const rect &frame, int width, int height, int x, int y) {
		const pixel3f _black(0.f, 0.f, 0.f);

		pixel3f neighbourPattern = _black;
		double diff;
		int xLeft, xRight, yTop, yBottom;
		int right, bottom;
		xLeft = x - 1; xRight = x + frame.right + 1;
		yTop = y - 1; yBottom = y + frame.bottom + 1;
		right = x + frame.right;
		bottom = y + frame.bottom;

		for (int xx = x; xx < right; xx++) {
			int index;
			if (yTop >= 0) {
				index = yTop * width + xx;
				if (in[index] != _black) {
					diff = differenceF(color, in[index]);
					if (diff < HUNTER_LAB_THRESHOLD
						&& diff < differenceF(color, neighbourPattern))
						neighbourPattern = in[index];
				}

			}
			if (yBottom < height) {
				index = yBottom * width + xx;
				if (in[index] != _black) {
					diff = differenceF(color, in[index]);
					if (diff < HUNTER_LAB_THRESHOLD
						&& diff < differenceF(color, neighbourPattern))
						neighbourPattern = in[index];
				}
			}
		}

		for (int yy = y; yy < bottom; yy++) {
			int index;
			if (xLeft >= 0) {
				index = yy * width + xLeft;
				if (in[index] != _black) {
					diff = differenceF(color, in[index]);
					if (diff < HUNTER_LAB_THRESHOLD
						&& diff < differenceF(color, neighbourPattern))
						neighbourPattern = in[index];
				}

			}
			if (xRight < width) {
				index = yy * width + xRight;
				if (in[index] != _black) {
					diff = differenceF(color, in[index]);
					if (diff < HUNTER_LAB_THRESHOLD
						&& diff < differenceF(color, neighbourPattern))
						neighbourPattern = in[index];
				}
			}
		}
		return neighbourPattern;
	}

	__global__ void setMutex(int value) {
		mutex = value;
	}

}